package main

import (
	"flag"
	"log"
	"member/app"
)

var (
	runserver = flag.Bool("runserver", false, "This is a string argument for running server")
)

func main() {

	flag.Parse()

	if !*runserver {
		log.Fatalf("Please specify the file you want to execute")
	} else {
		app.Run()
	}

}
