package utilities

import (
	"errors"
	"member/internal/consts"
	"regexp"
	"strings"

	"github.com/badoux/checkmail"
	"github.com/ttacon/libphonenumber"
)

// ValidateEmail checks if the provided email address is valid.
func ValidateEmail(email string) bool {
	if strings.TrimSpace(email) == "" {
		return false // Email is empty
	}

	err := checkmail.ValidateFormat(email)
	return err == nil
}

// ValidateName checks if the provided name is valid (letters and spaces only).
func ValidateName(name string) bool {
	regex := regexp.MustCompile(`^[A-Za-z\s]+$`)
	return regex.MatchString(name)
}

// IsValidUUID checks if the input string is a valid UUID.
func IsValidUUID(id string) bool {
	if len(id) != 36 {
		return false
	}
	segmentLengths := []int{8, 4, 4, 4, 12}
	index := 0
	for _, length := range segmentLengths {
		if index > 0 && id[index-1] != '-' {
			return false
		}
		for _, c := range id[index : index+length] {
			if !((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F')) {
				return false
			}
		}
		index += length + 1
	}
	return true
}

// ValidatePassword checks the validity of a password.
func ValidatePassword(password string) error {
	// Check minimum length
	if len(password) < 8 {
		return errors.New("Password must be at least 8 characters long")
	}

	// Check for at least one uppercase letter
	hasUppercase := false
	for _, char := range password {
		if char >= 'A' && char <= 'Z' {
			hasUppercase = true
			break
		}
	}
	if !hasUppercase {
		return errors.New("Password must contain at least one uppercase letter")
	}

	// Check for at least one lowercase letter
	hasLowercase := false
	for _, char := range password {
		if char >= 'a' && char <= 'z' {
			hasLowercase = true
			break
		}
	}
	if !hasLowercase {
		return errors.New("Password must contain at least one lowercase letter")
	}

	// Check for at least one special character
	specialCharPattern := regexp.MustCompile(`[!@#$%^&*()_+{}\[\]:;<>,.?~]`)
	if !specialCharPattern.MatchString(password) {
		return errors.New("Password must contain at least one special character")
	}

	// Check for spaces
	if regexp.MustCompile(`\s`).MatchString(password) {
		return errors.New("Password cannot contain spaces")
	}

	return nil
}

// IsValidPhoneNumber checks if a given string represents a valid phone number in the specified region.
//
// This function validates whether the provided string resembles a valid phone number in the specified region.
// It first trims any leading or trailing white spaces from the input string and then parses it as a phone number
// using the default region provided. If the parsing is successful, it checks if the parsed number is valid.
//
// Parameters:
//
//	@phoneNumber (string): The string to be validated as a phone number.
//	@defaultRegion (string): The default region code (e.g., "US", "GB") to use for parsing the phone number.
//
// Returns:
//
//	bool: True if the input string resembles a valid phone number in the specified region, false otherwise.
func IsValidPhoneNumber(phoneNumber, defaultRegion string) bool {
	phoneNumber = strings.TrimSpace(phoneNumber)

	// Parse the phone number
	num, err := libphonenumber.Parse(phoneNumber, defaultRegion)
	if err != nil {
		return false
	}

	// Check if the parsed number is valid
	return libphonenumber.IsValidNumber(num)
}

// IsValidZIPCode for Validate zipcode
func IsValidZIPCode(zipcode string) bool {
	// Define a regular expression pattern for a general ZIP code.
	// This pattern allows for 5 or more digits but doesn't specify a hyphen.
	pattern := `^\d{5,}$`

	// Compile the regular expression pattern.
	regex := regexp.MustCompile(pattern)

	// Use the regex.MatchString() function to check if the ZIP code matches the pattern.
	return regex.MatchString(zipcode)
}

// ValidateNameLength is for validating length of the name
func ValidateMaximumNameLength(name string) bool {
	if len(name) > consts.MaximumNameLength {
		return false
	}
	return true
}
