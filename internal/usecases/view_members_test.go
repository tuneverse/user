package usecases

import (
	"context"
	"fmt"
	"member/internal/consts"
	"member/internal/entities"
	"testing"

	"member/internal/repo/mock"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/tuneverse/toolkit/core/logger"
)

func TestViewMembers(t *testing.T) {
	// Create a sample params object.
	params := entities.Params{
		Status:  "all",
		Page:    "1",
		Limit:   "10",
		SortBy:  "created_on",
		Partner: "-1",
		Role:    "-1",
		Search:  "",
	}

	// Create sample member data that the repository will return.
	sampleMemberData := []entities.ViewMembers{
		{
			MemberId: uuid.MustParse("980e783c-e664-452d-b1ff-30d2e7767023"),
			Name:     "John Doe",
			Role: struct {
				Id   int
				Name string
			}{
				Id:   1,
				Name: "Member",
			},
			PartnerName: "Partner1",
			Email:       "john@example.com",
			Country: struct {
				Code string
				Name string
			}{
				Code: "IN",
				Name: "INDIA",
			},
			Active:      true,
			AlbumCount:  0,
			TrackCount:  0,
			ArtistCount: 7,
		},
		{
			MemberId: uuid.MustParse("980e703c-e664-452d-b1ff-30d2e7767026"),
			Name:     "Sukanya P",
			Role: struct {
				Id   int
				Name string
			}{
				Id:   1,
				Name: "Member",
			},
			PartnerName: "Partner1",
			Email:       "sukanya@gmail.com",
			Country: struct {
				Code string
				Name string
			}{
				Code: "US",
				Name: "United States",
			},
			Active:      true,
			AlbumCount:  0,
			TrackCount:  0,
			ArtistCount: 0,
		},
	}

	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	logger.InitLogger(clientOpt)

	// Define test cases.
	testCases := []struct {
		name          string
		params        entities.Params
		buildStubs    func(mockMemberRepo *mock.MockMemberRepoImply)
		checkResponse func(t *testing.T, memberData []entities.ViewMembers, err error)
	}{
		{
			name:   "Successful Fetch",
			params: params,
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				// Expect the ViewMembers function to be called with the given params.
				mockMemberRepo.EXPECT().ViewMembers(gomock.Any(), params).
					Times(1).Return(sampleMemberData, nil)
			},
			checkResponse: func(t *testing.T, memberData []entities.ViewMembers, err error) {
				// Expect no errors and the returned member data to match the sample data.
				require.Nil(t, err)
				require.Equal(t, sampleMemberData, memberData)
			},
		},
		{
			name:   "Error Fetching Members",
			params: params,
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				// Expect the ViewMembers function to be called with the given params.
				mockMemberRepo.EXPECT().ViewMembers(gomock.Any(), params).
					Times(1).Return(nil, fmt.Errorf("Failed to fetch members"))
			},
			checkResponse: func(t *testing.T, memberData []entities.ViewMembers, err error) {
				// Expect an error and an empty member data slice.
				require.Error(t, err)
				require.Empty(t, memberData)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockMemberRepo := mock.NewMockMemberRepoImply(ctrl)
			tc.buildStubs(mockMemberRepo)

			memberUseCase := NewMemberUseCases(mockMemberRepo)
			memberData, err := memberUseCase.ViewMembers(context.Background(), tc.params)

			tc.checkResponse(t, memberData, err)
		})
	}
}
