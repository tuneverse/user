package usecases

import (
	"context"
	"member/internal/consts"
	"member/internal/entities"
	"member/internal/repo/mock"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/tuneverse/toolkit/core/logger"
)

func TestGetBasicMemberDetailsByEmail(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	args := entities.MemberPayload{
		PartnerID: uuid.New(),
		Email:     "john@example.com",
		Provider:  consts.ProviderInternal,
		Password:  "Pass@123",
	}

	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName,
		LogLevel:            "info",
		IncludeRequestDump:  false,
		IncludeResponseDump: false,
	}

	logger.InitLogger(clientOpt)

	testCases := []struct {
		name          string
		args          entities.MemberPayload
		buildStubs    func(mockMemberRepo *mock.MockMemberRepoImply)
		checkResponse func(t *testing.T, fieldsMap map[string][]string, basicData entities.BasicMemberData, err error)
	}{
		{
			name: "Valid Email and Password",
			args: args,
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				// Expectations for a valid email and password.
				mockMemberRepo.EXPECT().GetBasicMemberDetailsByEmail(args, gomock.Any()).Return(entities.BasicMemberData{}, nil)
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, basicData entities.BasicMemberData, err error) {
				require.Len(t, fieldsMap, 0) // No errors expected
				require.Nil(t, err)
			},
		},
		{
			name: "Empty Email",
			args: entities.MemberPayload{ // Empty email
				PartnerID: uuid.New(),
				Email:     "",
				Provider:  consts.ProviderInternal,
				Password:  "Pass@123",
			},
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				// No expectations as there should be no repository calls.
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, basicData entities.BasicMemberData, err error) {
				require.Len(t, fieldsMap, 1)
				require.Contains(t, fieldsMap, consts.Email)
				require.Nil(t, err)
			},
		},
		{
			name: "Valid Email and Short Password",
			args: entities.MemberPayload{
				PartnerID: uuid.New(),
				Email:     "john@example.com",
				Provider:  consts.ProviderInternal,
				Password:  "1234567", // Password is too short
			},
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				// No expectations as there should be no repository calls.
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, basicData entities.BasicMemberData, err error) {
				require.Len(t, fieldsMap, 1)
				require.Contains(t, fieldsMap, consts.Password)
				require.Nil(t, err)
			},
		},
		{
			name: "Valid Email and Invalid Password Format",
			args: entities.MemberPayload{
				PartnerID: uuid.New(),
				Email:     "john@example.com",
				Provider:  consts.ProviderInternal,
				Password:  "invalidpass", // Invalid password format
			},
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				// No expectations as there should be no repository calls.
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, basicData entities.BasicMemberData, err error) {
				require.Len(t, fieldsMap, 1)
				require.Contains(t, fieldsMap, consts.Password)
				require.Nil(t, err)
			},
		},
		{
			name: "Empty Email and Password",
			args: entities.MemberPayload{
				PartnerID: uuid.New(),
				Email:     "",
				Provider:  consts.ProviderInternal,
				Password:  "",
			},
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				// No expectations as there should be no repository calls.
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, basicData entities.BasicMemberData, err error) {
				require.Len(t, fieldsMap, 2)
				require.Contains(t, fieldsMap, consts.Email)
				require.Contains(t, fieldsMap, consts.Password)
				require.Nil(t, err)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]
		t.Run(tc.name, func(t *testing.T) {
			mockMemberRepo := mock.NewMockMemberRepoImply(ctrl)
			tc.buildStubs(mockMemberRepo)

			memberUseCase := NewMemberUseCases(mockMemberRepo)
			fieldsMap, basicData, err := memberUseCase.GetBasicMemberDetailsByEmail(context.Background(), tc.args, nil, "", "")

			tc.checkResponse(t, fieldsMap, basicData, err)
		})
	}
}
