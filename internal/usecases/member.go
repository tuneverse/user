package usecases

import (
	"context"
	"member/internal/consts"
	"member/internal/entities"

	"member/internal/repo"
	"member/utilities"

	"github.com/google/uuid"

	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/utils"
	cryptoHash "gitlab.com/tuneverse/toolkit/utils/crypto"
)

// MemberUseCases defines use cases related to member operations.
type MemberUseCases struct {
	repo repo.MemberRepoImply
}

// MemberUseCaseImply interface
type MemberUseCaseImply interface {
	// AddBillingAddress adds a billing address to a member's profile.
	// It takes the context, memberID, and billingAddress as input, and returns a map of validation error messages and an error, if any.
	AddBillingAddress(ctx context.Context, memberID uuid.UUID, billingAddress entities.BillingAddress) (map[string][]string, error)

	// UpdateBillingAddress updates an existing billing address in a member's profile.
	// It takes the context, memberID, and billingAddress as input, and returns a map of validation error messages and an error, if any.
	UpdateBillingAddress(ctx context.Context, memberID uuid.UUID, billingAddress entities.BillingAddress) (map[string][]string, error)

	// UpdateMember updates a member's profile.
	// It takes the context, memberID, and updated member profile details as input, and returns a map of validation error messages and an error, if any.
	UpdateMember(ctx context.Context, memberID uuid.UUID, args entities.Member) (map[string][]string, error)

	// GetAllBillingAddresses retrieves all billing addresses associated with a member.
	// It takes the context and memberID as input, and returns a slice of BillingAddress entities and an error, if any.
	GetAllBillingAddresses(ctx context.Context, memberID uuid.UUID) ([]entities.BillingAddress, error)

	// ChangePassword updates a member's password.
	// It takes the context, memberID, new password, and current password as input, and returns a map of validation error messages and an error, if any.
	ChangePassword(ctx context.Context, memberID uuid.UUID, newPassword string, currentPassword string) (map[string][]string, error)

	// IsMemberExists checks if a member with the specified memberId exists.
	// It takes the memberId and context as input, and returns true if the member exists, false otherwise, and an error if one occurs.
	IsMemberExists(memberId uuid.UUID, ctx context.Context) (bool, error)

	// RegisterMember registers a new member, Parameters: ctxt, args, contextError, endpoint, method, Returns: Headers, Error
	RegisterMember(ctxt context.Context, args entities.Member, contextError map[string]any, endpoint string, method string) (map[string][]string, error)

	// ViewMemberProfile retrieves a member's profile,Parameters: ctxt, memberId, contextError,endpoint, method, Returns: Headers, MemberProfile, Error
	ViewMemberProfile(ctxt context.Context, memberId uuid.UUID, contextError map[string]any, endpoint string, method string) (map[string][]string, entities.MemberProfile, error)

	// ViewMembers retrieves a list of members, Parameters: ctxt, params, Returns: ViewMembers slice, Error
	ViewMembers(ctxt context.Context, params entities.Params) ([]entities.ViewMembers, error)

	// GetBasicMemberDetailsByEmail retrieves basic member details, Parameters: ctxt, memberId,contextError,endpoint,method Returns: Headers,BasicMemberData, Error
	GetBasicMemberDetailsByEmail(ctxt context.Context, args entities.MemberPayload, contextError map[string]any, endpoint string,
		method string) (map[string][]string, entities.BasicMemberData, error)
}

// NewMemberUseCases is a constructor for creating an instance of MemberUseCases.
func NewMemberUseCases(memberRepo repo.MemberRepoImply) MemberUseCaseImply {
	return &MemberUseCases{
		repo: memberRepo,
	}
}

// AddBillingAddress handles adding a billing address for a member.
//
// This function validates the required fields in the billing address, validates the ZIP code,
// and then calls the repository function to add the billing address.
//
// Parameters:
//   - ctxt: The context for the operation.
//   - memberID: The UUID of the member for whom the billing address is being added.
//   - billingAddress: The billing address details to be added.
//   - contextError: A map used to collect error messages related to context validation.
//   - endpoint: A string representing the endpoint used for logging purposes.
//   - method: A string representing the HTTP method used for logging purposes.
//
// Returns:
//   - If successful, it returns nil (no error).
//   - If any required fields are missing or there's an error in adding the billing address,
//     it returns an error with an appropriate error message.
func (member *MemberUseCases) AddBillingAddress(ctx context.Context, memberID uuid.UUID, billingAddress entities.BillingAddress) (map[string][]string, error) {

	// Validate the required fields in the billing address
	fieldsMap := map[string][]string{}
	//Checking if address is empty
	if billingAddress.Address == "" {
		utils.AppendValuesToMap(fieldsMap, consts.Address, consts.Required)
		logger.Log().WithContext(ctx).Errorf("Failed to add billing address: Missing required fields")
		return fieldsMap, nil
	}

	//Checking if zipcode is empty
	if billingAddress.Zipcode == "" {
		utils.AppendValuesToMap(fieldsMap, consts.Zipcode, consts.Required)
		logger.Log().WithContext(ctx).Errorf("Failed to add billing address: Missing required fields")
		return fieldsMap, nil
	}

	// Validate the ZIP code by checking if length is equal to or greater than 5
	if len(billingAddress.Zipcode) < 5 {
		utils.AppendValuesToMap(fieldsMap, consts.Zipcode, consts.Minimum)
		logger.Log().WithContext(ctx).Errorf("Failed to add billing address")
		if isValidZip := utilities.IsValidZIPCode(billingAddress.Zipcode); !isValidZip {
			utils.AppendValuesToMap(fieldsMap, consts.Zipcode, consts.Invalid)
		}
	}

	if len(fieldsMap) != 0 {
		logger.Log().WithContext(ctx).Errorf("Failed to add billing address")
		return fieldsMap, nil
	}
	// Call the repository function to add the billing address
	err := member.repo.AddBillingAddress(ctx, memberID, billingAddress)

	if err != nil {
		logger.Log().WithContext(ctx).Errorf("Failed to add billing address: %s", err.Error())
		return nil, err
	}

	return fieldsMap, nil
}

// UpdateMember updates a member's profile.
// Parameters:
//
//	@ ctx: The context for the database operation.
//	@ memberID: The UUID of the member whose profile is being updated.
//	@ args: The updated member profile details.
//
// Returns:
//
//	@ error: An error, if any, during the database operation.
//
// UpdateMember updates a member's profile.
// Parameters:
//
//	@ ctx: The context for the database operation.
//	@ memberID: The UUID of the member whose profile is being updated.
//	@ args: The updated member profile details.
//
// Returns:
//
//	@ fieldsMap: A map of validation errors.
//	@ error: An error, if any, during the database operation.
//
// UpdateMember updates a member's profile.
func (member *MemberUseCases) UpdateMember(ctx context.Context, memberID uuid.UUID, args entities.Member) (map[string][]string, error) {
	// Initialize a map to store validation errors
	fieldsMap := map[string][]string{}

	// Check for mandatory fields
	if args.FirstName == "" {
		utils.AppendValuesToMap(fieldsMap, consts.FirstName, consts.Required)
		logger.Log().WithContext(ctx).Errorf("Profile updation failed, validation error: No First name")
		return fieldsMap, nil
	}

	// Validate first name
	if !utilities.ValidateName(args.FirstName) {
		utils.AppendValuesToMap(fieldsMap, consts.FirstName, consts.Valid)
		logger.Log().WithContext(ctx).Errorf("Profile updation failed, validation error: Invalid First name")
		return fieldsMap, nil

	}
	// Validate last name
	if args.LastName == "" {
		utils.AppendValuesToMap(fieldsMap, consts.LastName, consts.Required)
		logger.Log().WithContext(ctx).Errorf("Profile updation failed, validation error: No Last name")
		return fieldsMap, nil
	}

	if !utilities.ValidateName(args.LastName) {
		utils.AppendValuesToMap(fieldsMap, consts.LastName, consts.Valid)
		logger.Log().WithContext(ctx).Errorf("Profile updation failed, validation error: invalid last  name")

	}

	// Validate ZIP code
	if args.Zipcode == "" {
		utils.AppendValuesToMap(fieldsMap, consts.Zipcode, consts.Required)
		logger.Log().WithContext(ctx).Errorf("Profile updation failed, validation error: No Zipcode")

	}

	if !utilities.IsValidZIPCode(args.Zipcode) {
		utils.AppendValuesToMap(fieldsMap, consts.Zipcode, consts.Invalid)
		logger.Log().WithContext(ctx).Errorf("Profile updation failed, validation error: Invalid zip format")

	}
	// Validate state
	if args.State == "" {
		utils.AppendValuesToMap(fieldsMap, consts.State, consts.Required)
		logger.Log().WithContext(ctx).Errorf("Profile updation failed, validation error: No State")

	}

	// Validate phone number
	if !utilities.IsValidPhoneNumber(args.Phone, args.Country) {
		utils.AppendValuesToMap(fieldsMap, consts.PhoneNumber, consts.Format)
		logger.Log().WithContext(ctx).Errorf("Profile updation failed, validation error: No Zipcode")

	}
	if len(fieldsMap) != 0 {
		logger.Log().WithContext(ctx).Errorf("Profile updation failed, validation error")
		return fieldsMap, nil
	}

	// Perform the update operation
	err := member.repo.UpdateMember(ctx, memberID, args)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("Profile Updation failed, internal server error: %s", err.Error())
		return nil, err // Return the error here
	}

	return nil, nil
}

// GetAllBillingAddresses retrieves all billing addresses associated with a member.
// Parameters:
//
//	@ ctx: The context for the database operation.
//	@ memberID: The UUID of the member for whom billing addresses are being retrieved.
//
// Returns:
//
//	@ []entities.BillingAddress: A slice of billing addresses associated with the member.
//	@ error: An error, if any, during the retrieval process.
func (member *MemberUseCases) GetAllBillingAddresses(ctx context.Context, memberID uuid.UUID) ([]entities.BillingAddress, error) {
	// Call the repository method to retrieve all billing addresses
	billingAddresses, err := member.repo.GetAllBillingAddresses(ctx, memberID)
	if err != nil {
		return nil, err
	}
	return billingAddresses, nil
}

// UpdateBillingAddress handles updating a billing address for a member.
// ChangePassword updates a member's password.
//
// This function checks for missing required fields in the billing address, validates the ZIP code,
// and then calls the repository function to update the billing address.
//
// Parameters:
//
//	@ ctxt: The context for the operation.
//	@ memberID: The UUID of the member for whom the billing address is being updated.
//	@ billingAddress: The updated billing address details.
//	@ contextError: A map used to collect error messages related to context validation.
//	@ endpoint: A string representing the endpoint used for logging purposes.
//	@ method: A string representing the HTTP method used for logging purposes.
//
// Returns:
//   - If successful, it returns nil (no error).
//   - If any required fields are missing or there's an error in updating the billing address,
//     it returns an error with an appropriate error message.
func (member *MemberUseCases) UpdateBillingAddress(ctx context.Context, memberID uuid.UUID, billingAddress entities.BillingAddress) (map[string][]string, error) {

	// Validate the required fields in the billing address
	fieldsMap := map[string][]string{}
	//Checking if address is empty
	if billingAddress.Address == "" {
		utils.AppendValuesToMap(fieldsMap, consts.Address, consts.Required)
		logger.Log().WithContext(ctx).Errorf("Failed to add billing address: Missing required fields")
		return fieldsMap, nil
	}
	//Checking if zipcode is empty
	if billingAddress.Zipcode == "" {
		utils.AppendValuesToMap(fieldsMap, consts.Zipcode, consts.Required)
		logger.Log().WithContext(ctx).Errorf("Failed to add billing address: Missing required fields")
		return fieldsMap, nil
	}

	// Validate the ZIP code
	if len(billingAddress.Zipcode) < 5 {
		utils.AppendValuesToMap(fieldsMap, consts.Zipcode, consts.Minimum)
		logger.Log().WithContext(ctx).Errorf("Failed to add billing address")
		if isValidZip := utilities.IsValidZIPCode(billingAddress.Zipcode); !isValidZip {
			utils.AppendValuesToMap(fieldsMap, consts.Zipcode, consts.Invalid)
		}
	}
	if len(fieldsMap) != 0 {
		logger.Log().WithContext(ctx).Errorf("Failed to add billing address")
	}

	if len(fieldsMap) != 0 {
		logger.Log().WithContext(ctx).Errorf("Failed to update billing address")
	}

	// Call the repository function to add the billing address
	err := member.repo.UpdateBillingAddress(ctx, memberID, billingAddress)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("Failed to update billing address: %s", err.Error())
		return nil, err
	}

	return fieldsMap, nil
}

// ChangePassword updates a member's password.
// This function validates the new password, checks if it meets the minimum length requirement,
// and verifies that it doesn't match the current password. It then hashes the new password,
// compares it with the stored hash of the current password, and updates the password in the database.
//
// Parameters:
//   - ctx: The context for the operation.
//   - memberID: The UUID identifying the member.
//   - newPassword: The new password to set.
//   - currentPassword: The current password for validation.
//
// Returns:
//   - A map of field names to error messages, if any validation errors occur.
//   - An error if there is a database operation failure or other error.
func (member *MemberUseCases) ChangePassword(ctx context.Context, memberID uuid.UUID, newPassword string, currentPassword string) (map[string][]string, error) {
	// Initialize a fields map to hold validation errors
	fieldsMap := map[string][]string{}

	// Validate the new password
	// Check if the new password is empty or its length is less than 8
	if len(newPassword) < 8 || newPassword == "" {
		utils.AppendValuesToMap(fieldsMap, consts.NewPassword, consts.MinLength)
		logger.Log().WithContext(ctx).Errorf("ChangePassword failed, validation error:Not long enough")
		return fieldsMap, nil
	}

	// Additional password format validation (you can customize this)
	if err := utilities.ValidatePassword(newPassword); err != nil {
		utils.AppendValuesToMap(fieldsMap, consts.NewPassword, consts.Format)
		logger.Log().WithContext(ctx).Errorf("ChangePassword failed, validation error:Doesnot satisfy required format")
		return fieldsMap, nil

	}

	// Retrieve the hashed current password from the repository
	hashedCurrentPassword, err := member.repo.GetPasswordHash(ctx, memberID)
	if err != nil {
		// Log the error if password retrieval fails
		logger.Log().WithContext(ctx).Errorf("ChangePassword failed, internal server error: %s", err.Error())

		// Return an empty fields map and the error
		return nil, err
	}

	// Check if the new password is the same as the current password
	if newPassword == currentPassword {
		utils.AppendValuesToMap(fieldsMap, consts.NewPassword, consts.InvalidPassword)
		logger.Log().WithContext(ctx).Errorf("ChangePassword failed, validation error: New password is the same as the current password")

		// Return the fields map without logging the error (already logged)
		return fieldsMap, nil
	}

	// Check if the hashed current password matches the provided current password
	hashedPassword, err := cryptoHash.Hash(currentPassword)
	if hashedPassword != hashedCurrentPassword {
		utils.AppendValuesToMap(fieldsMap, consts.CurrentPassword, consts.Incorrect)
		logger.Log().WithContext(ctx).Errorf("ChangePassword failed, validation error: Current password is incorrect")

		// Return the fields map without logging the error (already logged)
		return fieldsMap, nil
	}

	// Hash the new password
	hashedNewPassword, err := cryptoHash.Hash(newPassword)
	err = member.repo.UpdatePassword(ctx, memberID, hashedNewPassword)
	if err != nil {
		// Log the error if password update fails
		logger.Log().WithContext(ctx).Errorf("ChangePassword failed, internal server error: Failed to update password: %s", err.Error())

		// Return an empty fields map and the error
		return nil, err
	}

	// Return the fields map (if there are any validation errors, it will be non-empty)
	return fieldsMap, nil
}

// IsMemberExists checks if a member with the given memberId exists.
// Parameters:
//
//	@ memberId: The UUID of the member to check for existence.
//	@ ctx: The context for the database operation.
//
// Returns:
//
//	@ bool: True if the member exists, false otherwise.
//	@ error: An error, if any, during the check.
func (member *MemberUseCases) IsMemberExists(memberId uuid.UUID, ctx context.Context) (bool, error) {
	_, err := member.repo.IsMemberExists(memberId, ctx)
	if err != nil {
		return false, err
	}
	return true, nil
}

// RegisterMember registers a new member and validates the input fields.
//
// Parameters:
//   - ctxt (context.Context): The context for the database operation.
//   - args (entities.Member): The member data to be registered.
//   - contextError (map[string]any): A map containing context-specific error responses.
//   - endpoint (string): The endpoint name.
//   - method (string): The HTTP request method (e.g., "POST" or "PUT").
//
// Returns:
//   - fieldsMap (map[string][]string): A map of validation error messages.
//   - error: An error, if any, encountered during the registration process.
func (member *MemberUseCases) RegisterMember(ctxt context.Context, args entities.Member, contextError map[string]any, endpoint string,
	method string) (map[string][]string, error) {

	fieldsMap := map[string][]string{}

	if args.FirstName != "" && !utilities.ValidateMaximumNameLength(args.FirstName) {
		utils.AppendValuesToMap(fieldsMap, consts.FirstName, consts.Length)
	}

	if args.FirstName != "" && !utilities.ValidateName(args.FirstName) {
		utils.AppendValuesToMap(fieldsMap, consts.FirstName, consts.Valid)
	}

	if args.LastName != "" && !utilities.ValidateMaximumNameLength(args.LastName) {
		utils.AppendValuesToMap(fieldsMap, consts.LastName, consts.Length)
	}

	if args.LastName != "" && !utilities.ValidateName(args.LastName) {
		utils.AppendValuesToMap(fieldsMap, consts.LastName, consts.Valid)
	}

	if args.Email == "" {
		utils.AppendValuesToMap(fieldsMap, consts.Email, consts.Required)
	}

	if !utilities.ValidateEmail(args.Email) {
		utils.AppendValuesToMap(fieldsMap, consts.Email, consts.Valid)
	}

	emailExists, err := member.repo.CheckEmailExists(ctxt, args.Email)

	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("Failed to check an email exists: err=%s", err.Error())
		return nil, err
	}

	if emailExists {
		utils.AppendValuesToMap(fieldsMap, consts.Email, consts.Exists)
	}

	if args.Provider == consts.ProviderInternal && args.Password == "" {
		utils.AppendValuesToMap(fieldsMap, consts.Password, consts.Required)
	}

	if args.Provider == consts.ProviderInternal && len(args.Password) < 8 {
		utils.AppendValuesToMap(fieldsMap, consts.Password, consts.MinLengthPassword)
	}

	if passwordErr := utilities.ValidatePassword(args.Password); args.Provider == consts.ProviderInternal && passwordErr != nil {
		utils.AppendValuesToMap(fieldsMap, consts.Password, consts.Format)
	}

	if args.Provider == consts.ProviderInternal && !args.TermsConditionChecked {
		utils.AppendValuesToMap(fieldsMap, consts.TermsAndConditions, consts.Required)
	}

	if args.Provider == consts.ProviderInternal && !args.PayingTax {
		utils.AppendValuesToMap(fieldsMap, consts.PayingTax, consts.Required)
	}

	if len(fieldsMap) == 0 {
		err := member.repo.RegisterMember(ctxt, args)
		if err != nil {
			logger.Log().WithContext(ctxt).Errorf("Failed to register a member: err=%s", err.Error())
			return nil, err
		}
	}

	return fieldsMap, nil
}

// ViewMemberProfile retrieves a member's profile and checks if the member exists.
//
// Parameters:
//   - ctxt (context.Context): The context for the database operation.
//   - memberId (uuid.UUID): The UUID of the member for whom to retrieve the profile.
//   - contextError (map[string]any): A map containing context-specific error responses.
//   - endpoint (string): The endpoint name.
//   - method (string): The HTTP request method (e.g., "GET" or "POST").
//
// Returns:
//   - fieldsMap (map[string][]string): A map of validation error messages.
//   - memberProfile (entities.MemberProfile): A struct containing the member's profile.
//   - error: An error, if any, encountered during the database operation.
func (member *MemberUseCases) ViewMemberProfile(ctxt context.Context, memberId uuid.UUID, contextError map[string]any, endpoint string,
	method string) (map[string][]string, entities.MemberProfile, error) {
	fieldsMap := map[string][]string{}
	var memberProfile entities.MemberProfile

	exists, err := member.repo.IsMemberExists(memberId, ctxt)

	if !exists {
		utils.AppendValuesToMap(fieldsMap, consts.MemberID, consts.Exists)
	}

	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("Checking member exists failed: err=%s", err.Error())
		return nil, memberProfile, err
	}

	if len(fieldsMap) == 0 {
		memberProfile, err = member.repo.ViewMemberProfile(memberId, ctxt)
		if err != nil {
			logger.Log().WithContext(ctxt).Errorf("View member profile failed: %s", err.Error())
			return nil, memberProfile, err
		}

	}
	return fieldsMap, memberProfile, nil
}

// ViewMembers retrieves a list of member profiles with additional information,
// such as their roles, partner names, album, track, and artist counts, from the database.
//
// Parameters:
//   - ctxt (context.Context): The context for the database operation.
//   - params (entities.Params): An instance of entities.Params containing filtering and pagination parameters.
//
// Returns:
//   - memberData ([]entities.ViewMembers): A slice of entities.ViewMembers, each representing a member's profile.
//   - error: An error, if any, encountered during the database operation.
//

func (member *MemberUseCases) ViewMembers(ctxt context.Context, params entities.Params) ([]entities.ViewMembers, error) {
	var memberData []entities.ViewMembers
	memberData, err := member.repo.ViewMembers(ctxt, params)
	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("Failed to fetch members data, err=%s", err.Error())
		return memberData, err
	}
	return memberData, nil
}

// GetBasicMemberDetailsByEmail retrieves basic details of a member based on their email.
//
// This function performs validation on the input arguments and returns any validation errors
// in a map, along with the basic member details and an error if the database operation fails.
//
// Parameters:
//   - ctxt (context.Context): The context for the database operation.
//   - args (entities.MemberPayload): An instance of entities.MemberPayload containing member information.
//   - contextError (map[string]any): A map to collect validation errors.
//   - endpoint (string): The endpoint for the request.
//   - method (string): The HTTP method used for the request.
//
// Returns:
//   - fieldsMap (map[string][]string): A map containing validation errors, if any, for different fields.
//   - memberBasic (entities.BasicMemberData): An instance of entities.BasicMemberData representing basic member details.
//   - error: An error, if any, encountered during the database operation.
func (member *MemberUseCases) GetBasicMemberDetailsByEmail(ctxt context.Context, args entities.MemberPayload, contextError map[string]any, endpoint string,
	method string) (map[string][]string, entities.BasicMemberData, error) {
	fieldsMap := map[string][]string{}
	var (
		memberBasic entities.BasicMemberData
		err         error
	)

	if args.Email == "" {
		utils.AppendValuesToMap(fieldsMap, consts.Email, consts.Required)
	}

	if !utilities.ValidateEmail(args.Email) {
		utils.AppendValuesToMap(fieldsMap, consts.Email, consts.Valid)
	}

	if args.Provider == consts.ProviderInternal && args.Password == "" {
		utils.AppendValuesToMap(fieldsMap, consts.Password, consts.Required)
	}

	if args.Provider == consts.ProviderInternal && len(args.Password) < 8 {
		utils.AppendValuesToMap(fieldsMap, consts.Password, consts.MinLengthPassword)
	}

	if passwordErr := utilities.ValidatePassword(args.Password); args.Provider == consts.ProviderInternal && passwordErr != nil {
		utils.AppendValuesToMap(fieldsMap, consts.Password, consts.Format)
	}

	if len(fieldsMap) == 0 {
		memberBasic, err = member.repo.GetBasicMemberDetailsByEmail(args, ctxt)
		if err != nil {
			logger.Log().WithContext(ctxt).Errorf("View basic member details failed: %s", err.Error())
			return nil, memberBasic, err
		}

	}

	return fieldsMap, memberBasic, nil
}
