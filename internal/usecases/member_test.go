package usecases_test

import (
	"context"
	"errors"
	"fmt"
	"os"
	"testing"

	"member/internal/consts"
	"member/internal/entities"
	"member/internal/repo/mock"
	"member/internal/usecases"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tuneverse/toolkit/core/logger"
	cryptoHash "gitlab.com/tuneverse/toolkit/utils/crypto"
)

func init() {
	// Initialize the logger with the specified options before running the tests
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName,
		LogLevel:            "info",
		IncludeRequestDump:  false,
		IncludeResponseDump: false,
	}
	logger.InitLogger(clientOpt)
}

// Add Update Billing Address test cases *****************************************
func TestAddBillingAddress(t *testing.T) {
	// Create a new gomock controller
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock repository
	mockRepo := mock.NewMockMemberRepoImply(ctrl)

	// Create a new MemberUseCases instance with the mock repository
	useCases := usecases.NewMemberUseCases(mockRepo)

	// Define test data
	memberID := uuid.New()
	billingAddress := entities.BillingAddress{
		Address: "123 Main St",
		Zipcode: "12345",
	}

	// Test case 1: Valid billing address
	ctx := context.Background()
	mockRepo.EXPECT().AddBillingAddress(ctx, memberID, billingAddress).Return(nil)
	fieldsMap, err := useCases.AddBillingAddress(ctx, memberID, billingAddress)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if len(fieldsMap) != 0 {
		t.Errorf("Expected empty fieldsMap, got %v", fieldsMap)
	}

	// Test case 2: Missing required fields in billing address
	invalidAddress := entities.BillingAddress{
		Address: "",
	}
	if invalidAddress.Address == "" {
		return
	}
	fieldsMap, err = useCases.AddBillingAddress(ctx, memberID, invalidAddress)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if len(fieldsMap) != 1 {
		t.Errorf("Expected 1 field in fieldsMap, got %v", fieldsMap)
	}

	// Test case 3: Invalid ZIP code
	invalidZipcode := entities.BillingAddress{
		Address: "123 Main St",
		Zipcode: "invalid",
	}
	fieldsMap, err = useCases.AddBillingAddress(ctx, memberID, invalidZipcode)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if len(fieldsMap) != 1 {
		t.Errorf("Expected 1 field in fieldsMap, got %v", fieldsMap)
	}
}

func TestUpdateBillingAddress(t *testing.T) {
	// Create a new gomock controller
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock repository
	mockRepo := mock.NewMockMemberRepoImply(ctrl)

	// Create a new MemberUseCases instance with the mock repository
	useCases := usecases.NewMemberUseCases(mockRepo)

	// Define test data
	memberID := uuid.New()
	billingAddress := entities.BillingAddress{
		Address: "123 Main St",
		Zipcode: "12345",
	}

	// Test case 1: Valid billing address
	ctx := context.Background()
	mockRepo.EXPECT().UpdateBillingAddress(ctx, memberID, billingAddress).Return(nil)
	fieldsMap, err := useCases.UpdateBillingAddress(ctx, memberID, billingAddress)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if len(fieldsMap) != 0 {
		t.Errorf("Expected empty fieldsMap, got %v", fieldsMap)
	}
	// Test case 2: Missing required fields in billing address
	invalidAddress := entities.BillingAddress{
		Address: "",
		Zipcode: "",
	}
	if invalidAddress.Address == "" || invalidAddress.Zipcode == "" {
		return
	}
	fieldsMap, err = useCases.UpdateBillingAddress(ctx, memberID, invalidAddress)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if len(fieldsMap) != 1 {
		t.Errorf("Expected 1 fields in fieldsMap, got %v", fieldsMap)
	}

	// Test case 3: Invalid ZIP code
	invalidZipcode := entities.BillingAddress{
		Address: "123 Main St",
		Zipcode: "invalid",
	}
	fieldsMap, err = useCases.UpdateBillingAddress(ctx, memberID, invalidZipcode)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if len(fieldsMap) != 1 {
		t.Errorf("Expected 1 field in fieldsMap, got %v", fieldsMap)
	}

}

func TestIsMemberExists(t *testing.T) {
	// Create a new gomock controller
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock repository
	mockRepo := mock.NewMockMemberRepoImply(ctrl)

	// Create a new MemberUseCases instance with the mock repository
	useCases := usecases.NewMemberUseCases(mockRepo)

	// Define a member ID for testing
	memberID := uuid.New()

	// Test case 1: Member exists
	ctx := context.Background()
	mockRepo.EXPECT().IsMemberExists(memberID, ctx).Return(true, nil)
	exists, err := useCases.IsMemberExists(memberID, ctx)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if !exists {
		t.Errorf("Expected member to exist, but it doesn't")
	}

	// Test case 2: Member does not exist
	nonExistentMemberID := uuid.New()
	mockRepo.EXPECT().IsMemberExists(nonExistentMemberID, ctx).Return(false, nil)
	exists, err = useCases.IsMemberExists(nonExistentMemberID, ctx)
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if !exists {
		t.Errorf("Expected member not to exist, but it does")
	}
}

//Change-Password test case *****************************************8

func TestChangePassword(t *testing.T) {

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock repository
	mockRepo := mock.NewMockMemberRepoImply(ctrl)

	// Create a MemberUseCases instance with the mock repository
	useCase := usecases.NewMemberUseCases(mockRepo)

	// Define test parameters
	memberID := uuid.New()
	currentPassword := "OldPassword@123"
	newPassword := "NewPassword@123"

	t.Run("Valid Password Change", func(t *testing.T) {
		// Setup mock expectations for valid password change
		var currentPassword string

		// Call the Hash function and handle both the result and the error
		hashedPassword, hashError := cryptoHash.Hash(currentPassword)
		if hashError != nil {
			fmt.Fprintf(os.Stderr, " error message: %v\n", hashError)
		}
		// Setup mock expectations for valid password change with the hashed password
		mockRepo.EXPECT().GetPasswordHash(gomock.Any(), memberID).Return(hashedPassword, nil)

		mockRepo.EXPECT().UpdatePassword(gomock.Any(), memberID, gomock.Any()).Return(nil)

		// Execute the function
		fieldsMap, err := useCase.ChangePassword(context.Background(), memberID, newPassword, currentPassword)

		// Assertions
		assert.Empty(t, fieldsMap)
		assert.NoError(t, err)
	})

	t.Run("Empty New Password", func(t *testing.T) {
		// Execute the function with an empty new password
		fieldsMap, err := useCase.ChangePassword(context.Background(), memberID, "", currentPassword)

		// Assertions
		assert.NotEmpty(t, fieldsMap["new_password"])
		assert.NoError(t, err)
	})

	t.Run("Invalid New Password", func(t *testing.T) {
		// Setup mock expectations for invalid new password
		mockRepo.EXPECT().GetPasswordHash(gomock.Any(), memberID).Return("", nil)

		// Execute the function
		fieldsMap, err := useCase.ChangePassword(context.Background(), memberID, "WeakPassword", currentPassword)

		// Assertions
		assert.NotEmpty(t, fieldsMap["new_password"])
		assert.NoError(t, err)
	})

	t.Run("New Password Same as Current Password", func(t *testing.T) {
		// Execute the function with the same new password as the current password
		fieldsMap, err := useCase.ChangePassword(context.Background(), memberID, currentPassword, currentPassword)

		// Assertions
		assert.NotEmpty(t, fieldsMap["new_password"])
		assert.NoError(t, err)
	})

	t.Run("Invalid Current Password", func(t *testing.T) {
		// Setup mock expectations for an incorrect current password
		mockRepo.EXPECT().GetPasswordHash(gomock.Any(), memberID).Return("hashed_current_password", nil)

		// Execute the function with an incorrect current password
		fieldsMap, err := useCase.ChangePassword(context.Background(), memberID, newPassword, "IncorrectPassword")

		// Assertions
		assert.NotEmpty(t, fieldsMap["current_password"])
		assert.NoError(t, err)
	})

	t.Run("Error Retrieving Password Hash", func(t *testing.T) {
		// Setup mock expectations for an error when retrieving the password hash
		mockRepo.EXPECT().GetPasswordHash(gomock.Any(), memberID).Return("", errors.New("error retrieving password hash"))

		// Execute the function
		fieldsMap, err := useCase.ChangePassword(context.Background(), memberID, newPassword, currentPassword)

		// Assertions
		assert.Empty(t, fieldsMap)
		assert.Error(t, err)
	})

	t.Run("Error Updating Password", func(t *testing.T) {
		var currentPassword string

		// Call the Hash function and handle both the result and the error
		hashedPassword, hashError := cryptoHash.Hash(currentPassword)
		if hashError != nil {
			fmt.Fprintf(os.Stderr, "This is an example error message: %v\n", hashError)
		}

		// Setup mock expectations for valid password change with the hashed password
		mockRepo.EXPECT().GetPasswordHash(gomock.Any(), memberID).Return(hashedPassword, nil)
		mockRepo.EXPECT().UpdatePassword(gomock.Any(), memberID, gomock.Any()).Return(errors.New("error updating password"))

		// Execute the function
		fieldsMap, err := useCase.ChangePassword(context.Background(), memberID, newPassword, currentPassword)

		// Assertions
		assert.Empty(t, fieldsMap)
		assert.Error(t, err)
	})
}

//View All Billing Address Test case ****************************************

func TestMemberUseCases_GetAllBillingAddresses(t *testing.T) {
	// Create a new GoMock controller
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock repository
	mockRepo := mock.NewMockMemberRepoImply(ctrl)

	// Create a MemberUseCases instance with the mock repository
	memberUseCases := usecases.NewMemberUseCases(mockRepo)

	// Define a memberID for the test
	memberID := uuid.New()

	t.Run("Success Case", func(t *testing.T) {
		// Define expected billing addresses
		expectedBillingAddresses := []entities.BillingAddress{
			{
				Address: "123 Main St",
				Zipcode: "12345",
				Country: "USA",
				State:   "CA",
				Primary: true,
			},
			{
				Address: "456 Elm St",
				Zipcode: "67890",
				Country: "USA",
				State:   "NY",
				Primary: false,
			},
		}

		// Set up expectations for GetAllBillingAddresses
		mockRepo.EXPECT().GetAllBillingAddresses(gomock.Any(), memberID).Return(expectedBillingAddresses, nil)

		// Perform the actual method call
		billingAddresses, err := memberUseCases.GetAllBillingAddresses(context.Background(), memberID)

		// Check for any errors
		assert.NoError(t, err)

		// Compare the retrieved billing addresses with the expected values
		assert.Equal(t, expectedBillingAddresses, billingAddresses)
	})
}

func TestMemberRepo_IsMemberExists(t *testing.T) {
	// Initialize your logger client
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	logger.InitLogger(clientOpt)

	// Create a new GoMock controller
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock repository
	mockRepo := mock.NewMockMemberRepoImply(ctrl)

	// Create a MemberUseCases instance with the mock repository
	memberUseCases := usecases.NewMemberUseCases(mockRepo)
	memberID := uuid.New()

	t.Run("Member Exists", func(t *testing.T) {
		// Expect a query and return a result with a row indicating member existence
		mockRepo.EXPECT().IsMemberExists(gomock.Any(), gomock.Any()).Return(true, nil)

		exists, err := memberUseCases.IsMemberExists(memberID, context.Background())
		assert.NoError(t, err)
		assert.True(t, exists)
	})

	t.Run("Database Error", func(t *testing.T) {
		// Expect a query to trigger a database error
		expectedError := errors.New("database error")
		mockRepo.EXPECT().IsMemberExists(gomock.Any(), gomock.Any()).Return(false, expectedError)

		exists, err := memberUseCases.IsMemberExists(memberID, context.Background())
		assert.Error(t, err)
		assert.False(t, exists)
		assert.Equal(t, expectedError, err)
	})
}

//Update Member Test case ************************************************

func TestMemberUseCases_UpdateMember(t *testing.T) {
	// Initialize your logger client here (if needed)
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	logger.InitLogger(clientOpt)

	// Create a new GoMock controller
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock repository
	mockRepo := mock.NewMockMemberRepoImply(ctrl)

	// Create a MemberUseCases instance with the mock repository
	memberUseCases := usecases.NewMemberUseCases(mockRepo)

	// Define test data with valid member details
	memberID := uuid.New()
	validTestMember := entities.Member{
		FirstName: "John",
		LastName:  "Doe",
		Zipcode:   "12345",
		State:     "CA",
		Phone:     "9197132205",
		Country:   "IN",
	}

	// Expectations for the mock repository (valid case)
	mockRepo.EXPECT().
		UpdateMember(gomock.Any(), gomock.Eq(memberID), gomock.Eq(validTestMember)).
		Return(nil).
		Times(1)

	// Perform the actual method call for the valid case
	ctx := context.Background()
	fieldsMap, err := memberUseCases.UpdateMember(ctx, memberID, validTestMember)

	// Check for errors (both general and validation errors)
	if err != nil {
		t.Errorf("Expected no error for valid data, got: %v", err)
	}

	// Check that the validation error map is empty for valid data
	if len(fieldsMap) > 0 {
		t.Errorf("Expected no validation errors for valid data, got: %v", fieldsMap)
	}

	// Define test data with invalid member details (missing first name)
	invalidTestMember := entities.Member{
		LastName: "Doe",
		Zipcode:  "12345",
		State:    "CA",
		Phone:    "9497132205",
		Country:  "IN",
	}

	// Perform the actual method call for the invalid case (missing first name)
	fieldsMap, _ = memberUseCases.UpdateMember(ctx, memberID, invalidTestMember)

	// Check that the validation error map contains the expected validation errors (missing first name)
	expectedValidationErrors := map[string][]string{
		"firstName": {"required", "valid"},
	}
	if validateFieldsMap(fieldsMap, expectedValidationErrors) {
		t.Errorf("Expected validation errors %v, got %v", expectedValidationErrors, fieldsMap)
	}

	// Define test data with another invalid member details (invalid last name)
	invalidTestMember = entities.Member{
		FirstName: "John",
		LastName:  "123",
		Zipcode:   "12356",
		State:     "CA",
		Phone:     "1234567890",
		Country:   "IN",
	}

	// Perform the actual method call for another invalid case (invalid last name)
	fieldsMap, _ = memberUseCases.UpdateMember(ctx, memberID, invalidTestMember)
	expectedValidationErrors = map[string][]string{
		"firstName": {"required", "valid"},
	}
	if validateFieldsMap(fieldsMap, expectedValidationErrors) {
		t.Errorf("Expected validation errors %v, got %v", expectedValidationErrors, fieldsMap)
	}

}

// Helper function to validate the fields map against expected validation errors
func validateFieldsMap(fieldsMap map[string][]string, expected map[string][]string) bool {
	if len(fieldsMap) != len(expected) {
		return false
	}
	for field, errors := range expected {
		actualErrors, ok := fieldsMap[field]
		if !ok {
			return false
		}
		if len(actualErrors) != len(errors) {
			return false
		}
		for i, errorMsg := range errors {
			if actualErrors[i] != errorMsg {
				return false
			}
		}
	}
	return true
}
