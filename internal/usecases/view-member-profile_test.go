// member_usecases_test.go

package usecases

import (
	"context"
	"fmt"
	"member/internal/consts"
	"member/internal/entities"
	"testing"

	"member/internal/repo/mock"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/tuneverse/toolkit/core/logger"
)

func TestViewMemberProfile(t *testing.T) {
	// Create a sample member ID.
	memberID := uuid.New()

	memberProfile := entities.MemberProfile{
		MemberDetails: entities.Member{
			Title:     "Mr",
			FirstName: "John",
			LastName:  "Doe",
			Gender:    "M",
			Email:     "john@example.com",
			Phone:     "1234567890",
			Address1:  "123 Main St",
			Address2:  "",
			Country:   "IN",
			State:     "KL",
			City:      "New York",
			Zipcode:   "",
			Language:  "en",
		},
		EmailSubscribed: false,
		MemberBillingAddress: []entities.BillingAddress{
			{
				Address: "Anchal",
				Zipcode: "2177",
				Country: "IN",
				State:   "KL",
				Primary: true,
			},
		},
	}

	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	logger.InitLogger(clientOpt)

	// Define test cases.
	testCases := []struct {
		name          string
		memberID      uuid.UUID
		memberProfile entities.MemberProfile
		buildStubs    func(mockMemberRepo *mock.MockMemberRepoImply)
		checkResponse func(t *testing.T, fieldsMap map[string][]string, memberProfile entities.MemberProfile, err error)
	}{
		{
			name:          "Valid Member Profile",
			memberID:      memberID,
			memberProfile: memberProfile,
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				// Expect the IsMemberExists function to be called with the given member ID.
				mockMemberRepo.EXPECT().IsMemberExists(memberID, gomock.Any()).
					Times(1).Return(true, nil)

				// Expect the ViewMemberProfile function to be called with the given member ID.
				mockMemberRepo.EXPECT().ViewMemberProfile(memberID, gomock.Any()).
					Times(1).Return(memberProfile, nil)
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, memberProfile entities.MemberProfile, err error) {
				require.Nil(t, err)
				require.Empty(t, fieldsMap)
				require.NotNil(t, memberProfile)
			},
		},
		{
			name:          "Member Does Not Exist",
			memberID:      memberID,
			memberProfile: memberProfile,
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				// Expect the IsMemberExists function to be called with the given member ID.
				mockMemberRepo.EXPECT().IsMemberExists(memberID, gomock.Any()).
					Times(1).Return(false, nil)
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, memberProfile entities.MemberProfile, err error) {
				// Expect an error indicating that the member does not exist.
				require.Nil(t, err)
				require.Contains(t, fieldsMap, consts.MemberID)
				require.Empty(t, memberProfile)
			},
		},
		{
			name:          "Error Checking Member Existence",
			memberID:      memberID,
			memberProfile: memberProfile,
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				// Expect the IsMemberExists function to be called with the given member ID.
				mockMemberRepo.EXPECT().IsMemberExists(memberID, gomock.Any()).
					Times(1).Return(false, fmt.Errorf("Failed to check member existence"))
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, memberProfile entities.MemberProfile, err error) {
				// Expect an error indicating a failure to check member existence.
				require.Error(t, err)
				require.Empty(t, fieldsMap)
				require.Empty(t, memberProfile)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockMemberRepo := mock.NewMockMemberRepoImply(ctrl)
			tc.buildStubs(mockMemberRepo)

			memberUseCase := NewMemberUseCases(mockMemberRepo)
			fieldsMap, memberProfile, err := memberUseCase.ViewMemberProfile(context.Background(), tc.memberID, nil, "", "")

			tc.checkResponse(t, fieldsMap, memberProfile, err)
		})
	}
}
