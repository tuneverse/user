package usecases

import (
	"context"
	"member/internal/consts"
	"member/internal/entities"
	"testing"

	"member/internal/repo/mock"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/tuneverse/toolkit/core/logger"
)

func TestRegisterMember(t *testing.T) {
	// sample member entity for internal provider.
	member1 := entities.Member{
		FirstName:             "",
		LastName:              "",
		Email:                 "rahul@gmail.com",
		Password:              "Rahul@123",
		TermsConditionChecked: true,
		PayingTax:             true,
		Provider:              "internal",
	}

	// sample member entity for external provider loke google,facebook etc.
	member2 := entities.Member{
		Email:    "rahul@gmail.com",
		Provider: "google",
	}

	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	logger.InitLogger(clientOpt)

	testCases := []struct {
		name          string
		member        entities.Member
		buildStubs    func(mockMemberRepo *mock.MockMemberRepoImply)
		checkResponse func(t *testing.T, fieldsMap map[string][]string, err error)
	}{
		{
			name:   "Valid Registration with internal provider",
			member: member1,
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				mockMemberRepo.EXPECT().CheckEmailExists(gomock.Any(), member1.Email).
					Times(1).Return(false, nil)
				mockMemberRepo.EXPECT().RegisterMember(gomock.Any(), member1).
					Times(1).Return(nil)
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, err error) {
				require.Len(t, fieldsMap, 0)
				require.Nil(t, err)
			},
		},
		{
			name:   "Email Already Exists",
			member: member1,
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				mockMemberRepo.EXPECT().CheckEmailExists(gomock.Any(), member1.Email).
					Times(1).Return(true, nil)
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, err error) {
				require.Len(t, fieldsMap, 1)
				require.Contains(t, fieldsMap, "email")
				require.Nil(t, err)
			},
		}, {
			name:   "Valid Registration with Google Provider",
			member: member2,
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				mockMemberRepo.EXPECT().CheckEmailExists(gomock.Any(), member2.Email).
					Times(1).Return(false, nil)
				mockMemberRepo.EXPECT().RegisterMember(gomock.Any(), gomock.Any()).
					Times(1).Return(nil)
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, err error) {
				require.Len(t, fieldsMap, 0)
				require.Nil(t, err)
			},
		},
		{
			name: "Google Provider with Existing Email",
			member: entities.Member{
				Email:    "rahul@gmail.com",
				Provider: "google",
			},
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				mockMemberRepo.EXPECT().CheckEmailExists(gomock.Any(), "rahul@gmail.com").
					Times(1).Return(true, nil)
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, err error) {
				require.Len(t, fieldsMap, 1)
				require.Contains(t, fieldsMap, "email")
				require.Nil(t, err)
			},
		},
		{
			name: "Empty Password",
			member: entities.Member{
				FirstName:             "John",
				LastName:              "Doe",
				Email:                 "john@example.com",
				Password:              "", // Empty password.
				TermsConditionChecked: true,
				PayingTax:             true,
				Provider:              "internal",
			},
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				// Expect the CheckEmailExists function to be called with the given email.
				mockMemberRepo.EXPECT().CheckEmailExists(gomock.Any(), "john@example.com").
					Times(1).Return(false, nil)
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, err error) {
				require.Len(t, fieldsMap, 1)
				require.Contains(t, fieldsMap, "password")
				require.Nil(t, err)
			},
		},
		{
			name: "Registration with No Tax Payment",
			member: entities.Member{
				FirstName:             "John",
				LastName:              "Doe",
				Email:                 "john@example.com",
				Password:              "Pass@123",
				TermsConditionChecked: true,
				PayingTax:             false, // No tax payment.
				Provider:              "internal",
			},
			buildStubs: func(mockMemberRepo *mock.MockMemberRepoImply) {
				// Expect the CheckEmailExists function to be called with the given email.
				mockMemberRepo.EXPECT().CheckEmailExists(gomock.Any(), "john@example.com").
					Times(1).Return(false, nil)
			},
			checkResponse: func(t *testing.T, fieldsMap map[string][]string, err error) {
				require.Len(t, fieldsMap, 1)
				require.Contains(t, fieldsMap, "paying_tax")
				require.Nil(t, err)
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockUserRepo := mock.NewMockMemberRepoImply(ctrl)
			tc.buildStubs(mockUserRepo)

			userUseCase := NewMemberUseCases(mockUserRepo)
			uID, err := userUseCase.RegisterMember(context.Background(), tc.member, nil, "", "")

			tc.checkResponse(t, uID, err)
		})
	}
}
