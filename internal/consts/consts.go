package consts

// DatabaseType represents the type of the database, set to "postgres."
const DatabaseType = "postgres"

// AppName represents the name of the application, set to "member."
const AppName = "member"

// AcceptedVersions represents an accepted API version, set to "v1."
const AcceptedVersions = "v1"

// ContextAcceptedVersions represents the key for the accepted API versions in a context.
const ContextAcceptedVersions = "Accept-Version"

// ContextSystemAcceptedVersions represents the key for system-accepted API versions in a context.
const ContextSystemAcceptedVersions = "System-Accept-Versions"

// ContextAcceptedVersionIndex represents the key for the accepted API version index in a context.
const ContextAcceptedVersionIndex = "Accepted-Version-index"

// ContextErrorResponses represents the key for error responses in a context.
const ContextErrorResponses = "context-error-response"

// ContextLocallizationLanguage represents the key for localization language in a context.
const ContextLocallizationLanguage = "lan"

// HeaderLocallizationLanguage represents the key for the Accept-Language header.
const HeaderLocallizationLanguage = "Accept-Language"

// CacheErrorData represents the cache name for error data.
const CacheErrorData = "CACHE_ERROR_DATA"

// ExpiryTime represents the cache expiry time, set to 180 (seconds).
const ExpiryTime = 180

// ContextEndPoints represent
const ContextEndPoints = "context-endpoints"

// KeyNames
const (
	//Parse Error indicates some error in the json data trying to parse
	ParseErr = "Parse_error"

	// ValidationErr represents a validation error.
	ValidationErr = "validation_error"

	// ForbiddenErr represents a forbidden error.
	ForbiddenErr = "forbidden"

	// UnauthorisedErr represents an unauthorized error.
	UnauthorisedErr = "unauthorized"

	// NotFound represents a not found error.
	NotFound = "not_found"

	// InternalServerErr represents an internal server error.
	InternalServerErr = "internal_server_error"

	// Errors is a general category for error messages.
	Errors = "errors"

	// AllError represents all types of errors.
	AllError = "AllError"

	// Registration is related to user registration.
	Registration = "registration"

	// ErrorCode represents an error code.
	ErrorCode = "errorCode"

	// MemberIDErr represents an error related to "member_id".
	MemberIDErr = "member_id"

	// Required represents that a field is required.
	Required = "required"

	// FirstName represents a first name.
	FirstName = "firstname"

	// Invalid represents that a value is invalid.
	Invalid = "invalid"

	// LastName represents a last name.
	LastName = "lastname"

	// Email represents an email address.
	Email = "email"

	// EmailExists represents an error for an existing email address.
	EmailExists = "email_exists"

	// NewPassword represents a new password.
	NewPassword = "new_password"

	// CurrentPassword represents the current password.
	CurrentPassword = "current_password"

	// Format represents an error related to the format of a value.
	Format = "format"

	// InvalidPassword represents an error for an invalid password.
	InvalidPassword = "invalid_password"

	// Incorrect represents an error for something being incorrect.
	Incorrect = "incorrect"

	// Country represents a country.
	Country = "country"

	// State represents a state or region.
	State = "state"

	// Address represents an address.
	Address = "address"

	// City represents a city.
	City = "city"

	// Zipcode represents a ZIP code.
	Zipcode = "zipcode"

	// PhoneNumber represents a phone number.
	PhoneNumber = "phone"

	// Valid represents that a value is valid.
	Valid = "valid"

	//MinLength represents the minimum required length of password
	MinLength = "min"

	PostMemberRegistration = "post_member_registration"
	Length                 = "length"
	Exists                 = "exists"
	Password               = "password"
	MinLengthPassword      = "min_length"
	FormatUpperCase        = "format_uppercase"
	FormatLowerCase        = "format_lowercase"
	FormatSpecialCharacter = "format_specialcharacter"
	FormatSpace            = "format_space"
	TermsAndConditions     = "terms_and_conditions"
	PayingTax              = "paying_tax"
	GetMemberProfile       = "get_member_profile"
	MemberID               = "member_id"
)
const (
	// EndpointErr represents an error message related to loading endpoints from a service.
	EndpointErr = "Error occurred while loading endpoints from service"

	// ContextErr represents an error message related to loading an error from a service.
	ContextErr = "Error occurred while loading error from service"

	// Success is a success message for changing a password.
	Success = "Successfully changed password"

	// Successful is a success message for updating a profile.
	SuccessfullyUpdated = "Successfully Updated Profile"

	// SuccessfullyAdded is a success message for adding a billing address.
	SuccessfullyAdded = "Billing Address Added Successfully"

	// SuccessUpdated is a success message for updating a billing address.
	SuccessUpdated = "Billing Address Successfully Updated"

	//SuccessfullyListed is a success message for listing All Billing Address Succesfully
	SuccessfullyListed = "Billing Address Listed Successfully"

	// Mandatory represents a message for mandatory fields.
	Mandatory = "Mandatory Fields"

	//Minimum number of digits for zipcode
	Minimum = "minimum"

	// Active represents the status value for active members.
	Active = "active"
	// Inactive represents the status value for inactive members.
	Inactive = "inactive"

	// SuccessfullyRegistered is a message indicating successful member registration.
	SuccessfullyRegistered = "Member Registered successfully"

	// DefaultStatus is the default value for the "status" query parameter.
	DefaultStatus = "all"
	// DefaultPage is the default value for the "page" query parameter.
	DefaultPage = "1"
	// DefaultLimit is the default value for the "limit" query parameter.
	DefaultLimit = "10"
	// DefaultSortBy is the default value for the "sortby" query parameter.
	DefaultSortBy = "created_on"
	// DefaultPartner is the default value for the "partner" query parameter.
	DefaultPartner = "-1"
	// DefaultRole is the default value for the "role" query parameter.
	DefaultRole = "-1"
	// DefaultSearch is the default value for the "search" query parameter.
	DefaultSearch = ""
	// MaximumNameLength is the maximum length for the name.
	MaximumNameLength = 63
	// ProviderInternal represents the internal authentication provider.
	ProviderInternal = "internal"
)
