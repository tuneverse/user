package controllers

import (
	"fmt"
	"member/internal/consts"
	"member/internal/entities"
	"member/internal/usecases"

	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/core/version"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/utils"
)

// MemberController handles member-related HTTP requests and routes.
type MemberController struct {
	router   *gin.RouterGroup
	useCases usecases.MemberUseCaseImply
}

// NewMemberController creates a new instance of MemberController.
func NewMemberController(router *gin.RouterGroup, memberUseCase usecases.MemberUseCaseImply) *MemberController {
	return &MemberController{
		router:   router,
		useCases: memberUseCase,
	}
}

// InitRoutes initializes the routes for the MemberController.
//
// It sets up a route for the health check endpoint, where the version is a URL parameter.
// When a GET request is made to this endpoint, it calls the "HealthHandler" function.
//
// Params:
//
//	@member: required - the MemberController instance.

func (member *MemberController) InitRoutes() {
	member.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, member, "HealthHandler")
	})
	member.router.POST("/:version/members/:member_id/billing-address", func(ctx *gin.Context) {
		version.RenderHandler(ctx, member, "AddBillingAddress")
	})
	member.router.PATCH("/:version/members/:member_id/profile", func(ctx *gin.Context) {
		version.RenderHandler(ctx, member, "UpdateMember")
	})
	member.router.PATCH("/:version/members/:member_id/billing-address", func(ctx *gin.Context) {
		version.RenderHandler(ctx, member, "UpdateBillingAddress")
	})
	member.router.GET("/:version/members/:member_id/billing-address", func(ctx *gin.Context) {
		version.RenderHandler(ctx, member, "GetAllBillingAddresses")
	})
	member.router.PATCH("/:version/members/:member_id/change-password", func(ctx *gin.Context) {
		version.RenderHandler(ctx, member, "ChangePassword")
	})

	member.router.POST("/:version/members", func(ctx *gin.Context) {
		version.RenderHandler(ctx, member, "RegisterMember")
	})

	member.router.GET("/:version/members/:member_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, member, "ViewMemberProfile")
	})

	member.router.GET("/:version/members", func(ctx *gin.Context) {
		version.RenderHandler(ctx, member, "ViewMembers")
	})

	member.router.GET("/:version/members/oauth", func(ctx *gin.Context) {
		version.RenderHandler(ctx, member, "GetBasicMemberDetailsByEmail")
	})
}

// HealthHandler handles health check requests and responds with the server's health status.
//
// Params:
//
//	@ctx: required - the Gin context for the HTTP request.
func (member *MemberController) HealthHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server running with base version",
	})
}

// AddBillingAddress handles adding a billing address for a member.
// It performs the following steps:
//   - Validates the endpoint and method.
//   - Parses the member_id from the URL parameters.
//   - Binds the JSON request body to the  struct.

//   - Handles validation errors and internal server errors.
//   - Logs relevant information about the process.
//
// Parameters:
//
//	@ctx (*gin.Context): The Gin context for handling the HTTP request.

func (member *MemberController) AddBillingAddress(ctx *gin.Context) {
	// Retrieve and preprocess request details
	method := strings.ToLower(ctx.Request.Method)
	endpointUrl := ctx.FullPath()

	// Check if the endpoint exists in the context
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)

	// Check if the endpoint exists, if not, respond with a validation error.
	if !isEndpointExists {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Add Billing Address failed, endpoint does not exist in the database.")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	// Get the contextError map to handle error responses.
	contextError, errVal := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	fmt.Println(contextError)
	if !errVal {
		logger.Log().WithContext(ctx.Request.Context()).Error("Adding Billing Address failed, Failed to fetch error values from context")
		return
	}

	// Extract member_id from the URL parameters (UUID parsing)
	memberIDStr := ctx.Param("member_id")
	memberID, err := uuid.Parse(memberIDStr)

	// If parsing fails, log an error and return an appropriate JSON response
	if err != nil {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("AddBillingAddress failed, invalid member_id: %s", err.Error())
		val, hasError, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
		if !hasError {
			ctx.JSON(int(errorCode), val)
		}
		return
	}
	// Bind the JSON request body to the entities package's BillingAddress struct
	var billingAddress entities.BillingAddress
	if err := ctx.ShouldBindJSON(&billingAddress); err != nil {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("AddBillingAddress failed, invalid JSON data: %s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}

	// Call the use case to add a billing address
	fieldsMap, err := member.useCases.AddBillingAddress(ctx, memberID, billingAddress)

	if err != nil {
		if err.Error() == "Member does not exist" {
			// Member not exist
			logger.Log().WithContext(ctx.Request.Context()).Errorf("AddBillingAddress failed, Member doesnot exist %s", err.Error())
			val, hasError, errorCode := utils.ParseFields(ctx, consts.NotFound, "", contextError, "", "")
			if !hasError {
				ctx.JSON(int(errorCode), val)
			}
			return
		} else {
			// If an error occurs during the use case execution, log the error and return a 500 Internal Server Error response
			logger.Log().WithContext(ctx.Request.Context()).Errorf("AddBillingAddress failed, failed to add billing address: %s", err.Error())
			val, hasError, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
			if !hasError {
				ctx.JSON(int(errorCode), val)
			}
			return
		}
	}

	if len(fieldsMap) > 0 {
		fields := utils.FieldMapping(fieldsMap)
		logger.Log().WithContext(ctx.Request.Context()).Errorf("AddBillingAddress failed, failed to add billing address")
		val, hasError, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		if hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}
	// Billing address added successfully
	logger.Log().WithContext(ctx.Request.Context()).Info("AddBillingAddress: Billing address added successfully")

	// Data added successfully
	ctx.JSON(http.StatusCreated, gin.H{
		"message": consts.SuccessfullyAdded,
	})
}

// UpdateBillingAddress handles updating a billing address for a member.
// It handles updating a billing address for a member.
// It performs the following steps:
//   - Validates the endpoint and method.
//   - Parses the member_id from the URL parameters.
//   - Binds the JSON request body to the  struct.

//   - Handles validation errors and internal server errors.
//   - Logs relevant information about the process.
//
// Parameters:
//
//	@ctx (*gin.Context): The Gin context for handling the HTTP request.
func (member *MemberController) UpdateBillingAddress(ctx *gin.Context) {
	// Retrieve and preprocess request details
	method := strings.ToLower(ctx.Request.Method)
	endpointUrl := ctx.FullPath()

	// Check if the endpoint exists in the context
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)

	// Check if the endpoint exists, if not, respond with a validation error.
	if !isEndpointExists {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Update billing address failed ,endpoint does not exist in the database.")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}
	// Get the contextError map to handle error responses.
	contextError, errVal := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !errVal {
		logger.Log().WithContext(ctx.Request.Context()).Error("Updating Billing Address failed, Failed to fetch error values from context")
		return
	}
	// Extract member_id from the URL parameters (UUID parsing)
	memberIDStr := ctx.Param("member_id")
	memberID, err := uuid.Parse(memberIDStr)

	// If parsing fails, log an error and return an appropriate JSON response
	if err != nil {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Update BillingAddress failed, invalid member_id: %s", err.Error())
		val, hasError, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")

		if !hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	// Bind the JSON request body to the entities package's BillingAddress struct
	var updatedBillingAddress entities.BillingAddress
	if err := ctx.ShouldBindJSON(&updatedBillingAddress); err != nil {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Update BillingAddress failed, invalid JSON data: %s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}

	// Call the use case to update the billing address
	fieldsMap, err := member.useCases.UpdateBillingAddress(ctx, memberID, updatedBillingAddress)

	if err != nil {
		if err.Error() == "Member does not exist" {
			// Member not exist
			logger.Log().WithContext(ctx.Request.Context()).Errorf("Update BillingAddress failed, member not exist: %s", err.Error())
			val, hasError, errorCode := utils.ParseFields(ctx, consts.NotFound, "", contextError, "", "")
			if !hasError {
				ctx.JSON(int(errorCode), val)
				return
			}
		} else {
			// If an error occurs during the use case execution, log the error and return a 500 Internal Server Error response
			logger.Log().WithContext(ctx.Request.Context()).Errorf("Update BillingAddress failed : Internal server error %s", err.Error())
			val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	if len(fieldsMap) > 0 {
		fields := utils.FieldMapping(fieldsMap)
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Update BillingAddress:Validation error  ")
		val, hasError, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)

		if hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	// Billing address updated successfully
	logger.Log().WithContext(ctx.Request.Context()).Info("Update BillingAddress: Billing address updated successfully")

	// Data updated successfully
	ctx.JSON(http.StatusOK, gin.H{
		"message": consts.SuccessUpdated,
	})
}

// ChangePassword handles requests to change a member's password.
//
// This function handles password change requests and expects the following parameters:
//   - member_id (UUID): The unique identifier of the member whose password needs to be changed.
//   - NewPassword (string): The new password to set for the member.
//   - CurrentPassword (string): The current password to validate the change.
//
// It performs the following steps:
//   - Validates the endpoint and method.
//   - Parses the member_id from the URL parameters.
//   - Binds the JSON request body to the passwordChangeRequest struct.
//   - Calls the ChangePassword use case with the memberID and new password.
//   - Handles validation errors and internal server errors.
//   - Logs relevant information about the process.
//
// Parameters:
//
//	@ctx (*gin.Context): The Gin context for handling the HTTP request.
//
// HTTP Request:
//
//	POST /members/:member_id/change-password
//
// JSON Request Body:
//
//	{
//	  "NewPassword": "string",
//	  "CurrentPassword": "string"
//	}
//
// Returns:
//   - If successful, it responds with HTTP status 201 (Created) and a success message.
//   - If any errors occur during parsing, binding, or processing, it responds with an appropriate error message and status.
func (member *MemberController) ChangePassword(ctx *gin.Context) {
	// Get the HTTP request method (GET, POST, etc.) and convert it to lowercase.
	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	// ctxt := ctx.Request.Context()

	// Get the full endpoint URL of the request.
	endpointURL := ctx.FullPath()

	// Get the contextEndpoints map to check if the endpoint exists.
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)

	// Get the endpoint details based on the URL and HTTP method.
	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, method)

	// Check if the endpoint exists, if not, respond with a validation error.
	if !isEndpointExists {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("ChangePassword failed, validation error, endpoint does not exist in the database.")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	// Get the contextError map to handle error responses.
	contextError, errVal := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !errVal {
		logger.Log().WithContext(ctx.Request.Context()).Error("Change Password failed, Failed to fetch error values from context")
		return
	}

	// Parse the member_id from the URL parameters.
	memberIDStr := ctx.Param("member_id")
	memberID, err := uuid.Parse(memberIDStr)

	// Check for errors in member_id parsing.
	if err != nil {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Password changing failed, invalid member_id: %s", err.Error())
		val, hasError, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
		if !hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	// Bind the JSON request body to the passwordChangeRequest struct.
	var passwordChangeRequest entities.PasswordChangeRequest
	if err := ctx.BindJSON(&passwordChangeRequest); err != nil {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Password change failed, invalid JSON data: %s", err.Error())

		ctx.JSON(http.StatusBadRequest, gin.H{
			"error":  "Invalid JSON data",
			"errors": err.Error(),
		})
		return
	}

	// Call the ChangePassword use case with the memberID and new password.
	fieldsMap, err := member.useCases.ChangePassword(ctx, memberID, passwordChangeRequest.NewPassword, passwordChangeRequest.CurrentPassword)

	// Handle errors during password change.
	if err != nil {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Password change failed: %s", err.Error())
		val, hasError, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
		if !hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	// Handle validation errors.
	if len(fieldsMap) > 0 {
		fields := utils.FieldMapping(fieldsMap)
		logger.Log().WithContext(ctx).Errorf("Password Changing failed, validation error, err = %s", fields)
		val, hasValue, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		// Check if the ParseFields function returned a value (hasValue).
		if hasValue {
			logger.Log().WithContext(ctx.Request.Context()).Errorf("ChangePassword failed,: %s", val)
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	// Log password change completion.
	logger.Log().WithContext(ctx.Request.Context()).Info("Password change: Successfull")

	// Respond with success status.
	ctx.JSON(http.StatusCreated, gin.H{
		"message": consts.Success,
	})
}

// GetAllBillingAddresses handles the HTTP request to add a billing address to a member's account.
// It validates the endpoint and method, parses the member_id from the URL parameters,
// and binds the JSON request body to the GetAllBillingAddresses struct.
// It then calls the AddBillingAddress use case with the extracted memberID and billingAddressRequest.
// It handles validation errors, "No record found" errors, and internal server errors.
// Finally, it responds with a success message if the billing address is added successfully.
//
// Parameters:
//
//	@ctx (*gin.Context): The Gin context for handling the HTTP request.
//
// Returns:
//   - If successful, it responds with HTTP status 200 (OK) and a success message.
//   - If any errors occur during validation, "No record found" errors, or processing, it responds with an appropriate error message and status.

func (controller *MemberController) GetAllBillingAddresses(ctx *gin.Context) {
	// Get the HTTP request method (GET, POST, etc.) and convert it to lowercase.
	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()

	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)

	// Attempt to retrieve the specific endpoint associated with the current request
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	_ = endpoint

	// If the endpoint does not exist, log an error and return a 400 Bad Request response
	if !isEndpointExists {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Viewing Billing Address Failed, validation error, endpoint does not exist in the database.")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	// Get the contextError map to handle error responses.
	contextError, errVal := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !errVal {
		logger.Log().WithContext(ctx.Request.Context()).Error("Viewing all Billing Address failed:Failed to load Context errors")
		return
	}

	memberIDStr := ctx.Param("member_id")
	memberID, err := uuid.Parse(memberIDStr)
	// If parsing fails, log an error and return an appropriate JSON response

	if err != nil {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("View All Billing Address: Invalid member_id: %s", err.Error())
		val, hasError, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")

		if hasError {

			ctx.JSON(int(errorCode), val)
			return
		}
	}

	// Call the GetAllBillingAddresses use case with the extracted memberID
	billingAddresses, err := controller.useCases.GetAllBillingAddresses(ctx, memberID)
	if err != nil {
		// Check if the error message contains "No record found"
		if strings.Contains(err.Error(), "No record found") {
			// Handle the "No record found" error
			logger.Log().WithContext(ctx.Request.Context()).Errorf("View All Billing Address: Failed to retrieve billing address, err=%s", err.Error())
			val, hasError, errorCode := utils.ParseFields(ctx, consts.NotFound, "", contextError, "", "")
			if !hasError {
				ctx.JSON(int(errorCode), val)
				return
			}
		} else {
			// Handle other errors, e.g., internal server error
			logger.Log().WithContext(ctx.Request.Context()).Errorf("View All Billing Address: Failed to retrieve billing address, err=%s", err.Error())
			val, hasError, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
			if !hasError {
				ctx.JSON(int(errorCode), val)
				return
			}
		}
	}

	// Send the response as JSON with a success message
	ctx.JSON(http.StatusOK, gin.H{
		"message":        consts.SuccessfullyListed,
		"BillingAddress": billingAddresses,
	})

	// Log the success message
	logger.Log().WithContext(ctx.Request.Context()).Info("View All Billing Address: Billing Address Listed successfully")
}

// RegisterMember is for registraion of member
// This endpoint expects a JSON payload representing the member's registration data.
// Request JSON Body:
//
//	-Member: The JSON object containing member details.
//
// Response:
//   - If successful, it returns a JSON response with status code 201 (Created).
//   - If there is an error in the request (e.g., invalid JSON) or processing, it returns an
//     error JSON response with the appropriate status code and error message.
func (member *MemberController) RegisterMember(ctx *gin.Context) {
	ctxt := ctx.Request.Context()
	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()

	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)

	// Check if the endpoint exists in the context
	if !isEndpointExists {
		logger.Log().WithContext(ctx).Errorf("Member registration failed,Invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	contextError, errVal := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !errVal {
		logger.Log().WithContext(ctx.Request.Context()).Error("Member registration failed:Failed to load Context errors")
		return
	}

	var reqIn entities.Member
	if err := ctx.BindJSON(&reqIn); err != nil {
		logger.Log().WithContext(ctx).Errorf("Member registration failed, Invalid JSON data, err=%s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}

	//Call the RegisterMember
	fieldMap, err := member.useCases.RegisterMember(ctxt, reqIn, contextError, endpoint, method)

	//Checks the length of fieldMap for checking is there any validation error reported or not.
	if len(fieldMap) != 0 {
		logger.Log().WithContext(ctx).Errorf("Member registration failed: validation error")
		// to build the field format.
		fields := utils.FieldMapping(fieldMap)
		val, hasError, errorCode := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method)
		if hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	if err != nil {
		//For logging error message
		logger.Log().WithContext(ctx).Errorf("Member registration failed: database error err=%s", err.Error())
		//Retrieve error based on the api's requirement from the errors which are stored in context
		val, hasError, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		if !hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	ctx.JSON(http.StatusCreated, consts.SuccessfullyRegistered)

	// Log the success message
	logger.Log().WithContext(ctx).Info("Member Registration: Member Registered successfully")
}

// UpdateMember is for registraion of member
// This endpoint expects a JSON payload representing the member's data updation.
// Request JSON Body:
//
//	-Member: The JSON object containing member details.
//
// Response:
//   - If successful, it returns a JSON response with status code 201 (Created).
//   - If there is an error in the request (e.g., invalid JSON) or processing, it returns an
//     error JSON response with the appropriate status code and error message.
func (member *MemberController) UpdateMember(ctx *gin.Context) {
	var memberID uuid.UUID
	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()

	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)

	// Check if the endpoint exists in the context
	if !isEndpointExists {
		logger.Log().WithContext(ctx).Errorf("Member profile updation failed,Invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	contextError, errVal := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !errVal {
		logger.Log().WithContext(ctx.Request.Context()).Error("Member profile updation  failed:Failed to load Context errors")
		return
	}

	var args entities.Member
	if err := ctx.BindJSON(&args); err != nil {
		logger.Log().WithContext(ctx).Errorf("Member profile updation failed, Invalid JSON data, err=%s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}

	//Call the RegisterMember
	fieldMap, err := member.useCases.UpdateMember(ctx, memberID, args)

	//Checks the length of fieldMap for checking is there any validation error reported or not.
	if len(fieldMap) != 0 {
		logger.Log().WithContext(ctx).Errorf("Member profile updation failed: validation error")
		fields := utils.FieldMapping(fieldMap)
		val, hasError, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		if hasError {
			logger.Log().WithContext(ctx).Errorf("Member profile updation failed")
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	if err != nil {
		//For logging error message
		logger.Log().WithContext(ctx).Errorf("Member profileupdation  failed: database error err=%s", err.Error())
		//Retrieve error based on the api's requirement from the errors which are stored in context
		val, hasError, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		if !hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	ctx.JSON(http.StatusCreated, consts.SuccessfullyUpdated)
	logger.Log().WithContext(ctx).Info("Member Profile Updated: Profile successfully Updated ")
}

// ViewMemberProfile retrieves a member's profile based on the provided member_id.
// Parameters:
//
//	- ctx (gin.Context): The Gin context for handling the HTTP request.
//	- memberID: The unique identifier of the member to retrieve.
//
// Returns:
//  - memberProfile: The member's profile information.
//	- error: An error, if any, during the database operation.

func (member *MemberController) ViewMemberProfile(ctx *gin.Context) {
	// Retrieve and preprocess request details
	method := strings.ToLower(ctx.Request.Method)
	endpointUrl := ctx.FullPath()
	ctxt := ctx.Request.Context()

	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)

	// Check if the endpoint exists in the context
	if !isEndpointExists {
		logger.Log().WithContext(ctx).Errorf("View Member Profile:Invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	contextError, errVal := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !errVal {
		logger.Log().WithContext(ctx.Request.Context()).Error("View Member Profile failed:Failed to load Context errors")
		return
	}

	// Extract member_id from the URL parameters (UUID parsing)
	fieldsMap := map[string][]string{}
	memberIDStr := ctx.Param("member_id")
	memberID, err := uuid.Parse(memberIDStr)

	if err != nil {
		logger.Log().WithContext(ctx).Errorf("View Member Profile failed-invalid member_id: err=%s", err.Error())
		utils.AppendValuesToMap(fieldsMap, consts.MemberID, consts.Invalid)
		fields := utils.FieldMapping(fieldsMap)
		val, hasError, errorCode := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method)
		if hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	fieldMap, memberProfile, err := member.useCases.ViewMemberProfile(ctxt, memberID, contextError, endpoint, method)

	//Checks the length of fieldMap for checking is there any validation error reported or not.
	if len(fieldMap) != 0 {
		logger.Log().WithContext(ctx).Errorf("View Member Profile failed:  validation error")
		// to build the field format.
		fields := utils.FieldMapping(fieldMap)
		val, hasError, errorCode := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method)
		if hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	//Checks any database error reported or not.
	if err != nil {
		//For logging error message
		logger.Log().WithContext(ctx).Errorf("View Member Profile failed: err=%s", err.Error())
		//Retrieve error based on the api's requirement from the errors which are stored in context
		val, hasError, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		if !hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	ctx.JSON(http.StatusOK, gin.H{"message": "Member profile details retreived successfully", "data": memberProfile})
	// Log the success message
	logger.Log().WithContext(ctx).Info("View Member Profile: Member profile details retreived successfully")
}

// ViewMembers retrieves a list of members based on the provided query parameters.
//
// Parameters:
//   - ctx (gin.Context): The Gin context for handling the HTTP request.
//
// Returns:
//   - data ([]entities.Member): A list of member data matching the query parameters.
//   - error: An error, if any, during the database operation.

func (member *MemberController) ViewMembers(ctx *gin.Context) {
	ctxt := ctx.Request.Context()
	_, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)

	// Check if the endpoint exists in the context
	if !isEndpointExists {
		logger.Log().WithContext(ctx).Errorf("View Members failed:Invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	contextError, errVal := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !errVal {
		logger.Log().WithContext(ctx.Request.Context()).Error("View Members failed:Failed to load Context errors")
		return
	}

	// Parse query parameters and set default values
	var params entities.Params
	params.Status = ctx.DefaultQuery("status", consts.DefaultStatus)
	params.Page = ctx.DefaultQuery("page", consts.DefaultPage)
	params.Limit = ctx.DefaultQuery("limit", consts.DefaultLimit)
	params.SortBy = ctx.DefaultQuery("sortby", consts.DefaultSortBy)
	params.Partner = ctx.DefaultQuery("partner", consts.DefaultPartner)
	params.Role = ctx.DefaultQuery("role", consts.DefaultRole)
	params.Search = ctx.DefaultQuery("search", consts.DefaultSearch)

	// Call ViewMembers to retrieve member data
	membersData, err := member.useCases.ViewMembers(ctxt, params)

	if err != nil {
		//For logging error message
		logger.Log().WithContext(ctx).Errorf("View members failed: err=%s", err.Error())
		//Retrieve error based on the api's requirement from the errors which are stored in context
		val, hasError, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		if !hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	if len(membersData) == 0 {
		ctx.JSON(http.StatusOK, gin.H{"message": "No member details to show", "data": membersData})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": "Members details retreived successfully", "data": membersData})
	// Log the success message
	logger.Log().WithContext(ctx).Info("View members: Members details retreived successfully")
}

// GetBasicMemberDetailsByEmail retrieves the basic details of a member based on their email.
//
// Parameters:
//   - ctx (gin.Context): The Gin context for handling the HTTP request.
//
// Returns:
//   - None: The function sends a JSON response with the member's basic details.
func (member *MemberController) GetBasicMemberDetailsByEmail(ctx *gin.Context) {
	// Retrieve and preprocess request details
	method := strings.ToLower(ctx.Request.Method)
	endpointUrl := ctx.FullPath()
	ctxt := ctx.Request.Context()

	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)

	// Check if the endpoint exists in the context
	if !isEndpointExists {
		logger.Log().WithContext(ctx).Errorf("View Basic Member Details:Invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	contextError, errVal := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !errVal {
		logger.Log().WithContext(ctx.Request.Context()).Error("View Basic Member Details failed:Failed to load Context errors")
		return
	}

	var reqIn entities.MemberPayload
	if err := ctx.BindJSON(&reqIn); err != nil {
		logger.Log().WithContext(ctx).Errorf("Member registration failed, Invalid JSON data, err=%s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}

	fieldMap, basicMemberData, err := member.useCases.GetBasicMemberDetailsByEmail(ctxt, reqIn, contextError, endpoint, method)

	//Checks any database error reported or not.
	if err != nil {
		//For logging error message
		logger.Log().WithContext(ctx).Errorf("View Basic Member Details failed: err=%s", err.Error())
		//Retrieve error based on the api's requirement from the errors which are stored in context
		val, hasError, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		if hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	//Checks the length of fieldMap for checking is there any validation error reported or not.
	if len(fieldMap) != 0 {
		logger.Log().WithContext(ctx).Errorf("View Basic Member Details failed:  validation error")
		// to build the field format.
		fields := utils.FieldMapping(fieldMap)
		val, hasError, errorCode := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method)
		if hasError {
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	ctx.JSON(http.StatusOK, gin.H{"message": "Member basic details retreived successfully", "data": basicMemberData})
	// Log the success message
	logger.Log().WithContext(ctx).Info("View Basic Member Details: Member basic details retreived successfully")
}
