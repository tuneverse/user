package entities

import "github.com/google/uuid"

// Member struct to hold details of member profile
type Member struct {
	Title                 string `json:"title"`
	FirstName             string `json:"firstname"`
	LastName              string `json:"lastname"`
	Email                 string `json:"email"`
	Gender                string `json:"gender"`
	Language              string `json:"language"`
	Country               string `json:"country"`
	State                 string `json:"state"`
	Address1              string `json:"address1"`
	Address2              string `json:"address2"`
	City                  string `json:"city"`
	Zipcode               string `json:"zip"`
	Phone                 string `json:"phone"`
	TermsConditionChecked bool   `json:"terms_condition_checked"`
	PayingTax             bool   `json:"paying_tax"`
	Password              string `json:"password"`
	Provider              string `json:"provider,omitempty"`
}

// BillingAddress struct to hold details
type BillingAddress struct {
	Address string `json:"address" `
	Zipcode string `json:"zipcode" `
	Country string `json:"country"  `
	State   string `json:"state" `
	Primary bool   `json:"primary"`
}

// BillingAddressResponse response struct to hold details
type BillingAddressResponse struct {
	BillingAddress []BillingAddress `json:"billing_address"`
}

// PasswordChangeRequest represents a request to change a password.
type PasswordChangeRequest struct {
	CurrentPassword string `json:"current_password"`
	NewPassword     string `json:"new_password"`
}

// ErrorResponse represents an error response that can be sent back to clients in JSON format.
type ErrorResponse struct {
	Message   string                 `json:"message"`
	ErrorCode interface{}            `json:"errorCode"`
	Errors    map[string]interface{} `json:"errors"`
}

// MemberProfile combines MemberDetails and an array of BillingAddress
type MemberProfile struct {
	MemberDetails        Member           `json:"member_details"`
	MemberBillingAddress []BillingAddress `json:"member_billing_address"`
	EmailSubscribed      bool             `json:"email_subscribed"`
}

// Params represents a set of parameters that can be used for querying members.
type Params struct {
	Status  string
	Page    string
	Limit   string
	SortBy  string
	Partner string
	Role    string
	Search  string
}

// Role represents information about a specific role with an ID and name.
type Role struct {
	Id   int
	Name string
}

// Country represents information about a specific country with its code and name.
type Country struct {
	Code string
	Name string
}

// ViewMembers represents information about members with nested Role and Country data.
type ViewMembers struct {
	MemberId    uuid.UUID
	Name        string
	Role        Role
	PartnerName string
	Email       string
	Country     Country
	Active      bool
	AlbumCount  int
	TrackCount  int
	ArtistCount int
}

// BasicMemberData represents basic member details.
type BasicMemberData struct {
	MemberID    uuid.UUID `json:"member_id"`
	Name        string    `json:"member_name"`
	PartnerName string    `json:"partner_name"`
	PartnerID   uuid.UUID `json:"partner_id"`
	Email       string    `json:"member_email"`
	MemberType  string    `json:"member_type"`
	MemberRoles []string  `json:"member_roles"`
}

// MemberPayload represents essential information about a member.
type MemberPayload struct {
	PartnerID uuid.UUID `json:"partner_id"`
	Email     string    `json:"email"`
	Provider  string    `json:"provider"`
	Password  string    `json:"password"`
}
