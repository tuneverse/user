package repo

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"member/internal/entities"

	"member/internal/consts"
	"strconv"

	"github.com/google/uuid"

	"gitlab.com/tuneverse/toolkit/utils"

	"gitlab.com/tuneverse/toolkit/utils/crypto"
)

// MemberRepo defines a repository for member-related operations.
type MemberRepo struct {
	db *sql.DB
}

// MemberRepoImply represents the interface for interacting with the Member repository.
type MemberRepoImply interface {
	// AddBillingAddress adds a billing address for a member.
	AddBillingAddress(ctx context.Context, memberID uuid.UUID, billingAddress entities.BillingAddress) error

	// UpdateBillingAddress updates a billing address for a member.
	UpdateBillingAddress(ctx context.Context, memberID uuid.UUID, billingAddress entities.BillingAddress) error

	// IsMemberExists checks if a member with the given ID exists.
	IsMemberExists(memberID uuid.UUID, ctx context.Context) (bool, error)

	// GetAllBillingAddresses retrieves all billing addresses for a member.
	GetAllBillingAddresses(ctx context.Context, memberID uuid.UUID) ([]entities.BillingAddress, error)

	// UpdatePassword updates a member's password.
	UpdatePassword(ctx context.Context, memberID uuid.UUID, newPasswordHash string) error

	// GetPasswordHash retrieves a member's password hash.
	GetPasswordHash(ctx context.Context, memberID uuid.UUID) (string, error)

	// UpdateMember updates a member's profile.
	UpdateMember(ctx context.Context, memberID uuid.UUID, args entities.Member) error

	//CheckEmailExists checks email exists or not
	CheckEmailExists(ctx context.Context, email string) (bool, error)

	//RegisterMember registers a new member
	RegisterMember(ctx context.Context, args entities.Member) error

	//ViewMemberProfile retrieves a member profile details
	ViewMemberProfile(memberId uuid.UUID, ctx context.Context) (entities.MemberProfile, error)

	//ViewMembers retrieves all members details
	ViewMembers(ctx context.Context, params entities.Params) ([]entities.ViewMembers, error)

	GetBasicMemberDetailsByEmail(args entities.MemberPayload, ctx context.Context) (entities.BasicMemberData, error)
}

// NewMemberRepo creates a new instance of MemberRepo.
func NewMemberRepo(db *sql.DB) MemberRepoImply {
	return &MemberRepo{db: db}
}

// IsMemberExists checks if a member exists in the database.
// Parameters:
//	- memberID: The UUID of the member to check for existence.
//	- ctx: The context for the database operation.

// Returns:
// - bool: A boolean indicating whether the member exists.
// - error: An error, if any, during the database operation.
func (member *MemberRepo) IsMemberExists(memberId uuid.UUID, ctx context.Context) (bool, error) {
	var exists int
	//Checking if member with the passed ID exists
	isMemberExistsQ := `select 1 from member where id = $1`
	row := member.db.QueryRowContext(ctx, isMemberExistsQ, memberId)
	err := row.Scan(&exists)

	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}

	return true, nil
}

// AddBillingAddress inserts a new billing address for a member into the database.

// Parameters:
//	- ctx: The context for the database operation.
//	- memberID: The UUID of the member for whom the billing address is being added.
//	- billingAddress: The billing address details to be added.

// Returns:
//
//   - int: The result code (not used in this context).
//   - error: An error, if any, during the database operation.
func (member *MemberRepo) AddBillingAddress(ctx context.Context, memberID uuid.UUID, billingAddress entities.BillingAddress) error {

	// Check if the member exists
	memberExists, err := member.IsMemberExists(memberID, ctx)
	if err != nil {
		return fmt.Errorf("failed to check member existence: %v", err)
	}

	// If the member doesn't exist, return an error
	if !memberExists {
		return errors.New("Member does not exist")
	}

	// Check if the billing address already exists for the member.
	var exists int
	err = member.db.QueryRowContext(ctx, `
        SELECT COUNT(*)
        FROM member_billing_address
        WHERE member_id = $1 AND address = $2 AND zip = $3 AND country_code = $4 AND state_code = $5`,
		memberID, billingAddress.Address, billingAddress.Zipcode, billingAddress.Country, billingAddress.State).Scan(&exists)

	if err != nil {
		return fmt.Errorf("failed to check address existence: %v", err)
	}

	// If the billing address already exists, return an error
	if exists > 0 {
		return errors.New("This Billing address already exists for the member")
	}

	// If the member exists, the billing address doesn't exist, and there are no errors so far, proceed with insertion
	_, err = member.db.ExecContext(ctx, `
        INSERT INTO member_billing_address (member_id, address, zip, country_code, state_code, is_primary_billing)
        VALUES ($1, $2, $3, $4, $5, $6)`,
		memberID, billingAddress.Address, billingAddress.Zipcode, billingAddress.Country, billingAddress.State, billingAddress.Primary)

	if err != nil {
		return fmt.Errorf("failed to insert billing address: %v", err)
	}

	// If insertion is successful, return nil to indicate success
	return nil
}

// UpdateBillingAddress updates an existing billing address for a member in the database.

// Parameters:
//   - ctx: The context for the database operation.
//   - memberID: The UUID of the member for whom the billing address is being updated.
//   - billingAddress: The updated billing address details.
//
// Returns:
//   - error: An error, if any, during the database operation.
func (member *MemberRepo) UpdateBillingAddress(ctx context.Context, memberID uuid.UUID, billingAddress entities.BillingAddress) error {
	// Check if such a member exists
	memberExists, err := member.IsMemberExists(memberID, ctx)
	if err != nil {
		return err
	}
	if !memberExists {
		return errors.New("Member does not exist")
	}

	// If the member exists and provided fields are of correct value match, proceed with the updation
	_, err = member.db.ExecContext(ctx, `
        UPDATE member_billing_address
        SET address = $1, zip = $2, country_code = $3, state_code = $4, is_primary_billing = $5
        WHERE member_id = $6`,
		billingAddress.Address, billingAddress.Zipcode, billingAddress.Country, billingAddress.State, billingAddress.Primary, memberID,
	)

	if err != nil {
		return err
	}

	return nil
}

// UpdateMember updates a member's information in the repository.

// Parameters:
//   - ctx: The context for the database operation.
//   - memberID: The UUID of the member whose information is being updated.
//   - args: The updated member information.

// Returns:
//   - error: An error, if any, during the database operation.
func (member *MemberRepo) UpdateMember(ctx context.Context, memberID uuid.UUID, args entities.Member) error {
	// Check if the member with the given memberID exists
	checkMemberQry := `SELECT COUNT(*) FROM member WHERE id = $1`
	var memberExists int
	row := member.db.QueryRowContext(ctx, checkMemberQry, memberID)
	if err := row.Scan(&memberExists); err != nil {
		return err
	}
	// If the member exists, proceed with the profile updation with received values.
	updateQry := `UPDATE member SET
	title = $1, firstname = $2, lastname = $3, language_code = $4,
	country_code = $5, state_code = $6, address1 = $7, city= $8,zip= $9,mobile = $10,address2 = $11,
	WHERE id = $11`

	_, err := member.db.ExecContext(ctx, updateQry, args.Title, args.FirstName, args.LastName, args.Language,
		args.Country, args.State, args.Address1, args.City, args.Zipcode, args.Phone, memberID, args.Address2,
	)

	if err != nil {
		return err
	}
	return nil
}

// GetPasswordHash retrieves the password hash for a member.
// This function queries the database to fetch the password hash associated with a member's ID.
// Parameters:
//   - ctx: The context for the operation.
//   - memberID: The UUID of the member whose password hash is being retrieved.
//
// Returns:
//   - If successful, it returns the password hash as a string and nil (no error).
//   - If there's an error in the database operation, it returns an empty string and an error.
func (m *MemberRepo) GetPasswordHash(ctx context.Context, memberID uuid.UUID) (string, error) {
	var passwordHash string
	err := m.db.QueryRowContext(ctx, `
        SELECT password FROM member WHERE id = $1`, memberID).Scan(&passwordHash)
	if err != nil {
		return "", err
	}

	return passwordHash, nil
}

// UpdatePassword updates the password hash for a member.
// This function updates the password hash in the database for the specified member.
// Parameters:
//   - ctx: The context for the operation.
//   - memberID: The UUID of the member whose password hash is being updated.
//   - newPasswordHash: The new password hash to be set for the member.
//
// Returns:
//   - If successful, it returns nil (no error).
//   - If there's an error in the database operation, it returns an error.
func (m *MemberRepo) UpdatePassword(ctx context.Context, memberID uuid.UUID, newPasswordHash string) error {
	_, err := m.db.ExecContext(ctx, `
        UPDATE member SET password = $1 WHERE id = $2`, newPasswordHash, memberID)
	if err != nil {
		return err
	}
	return nil
}

// GetAllBillingAddresses retrieves all billing addresses associated with a member.
// If no billing addresses are found for the member, it returns an error "No record found."

// Parameters:
//   - ctx: The context for the database operation.
//   - memberID: The UUID of the member for whom billing addresses are being retrieved.
//
// Returns:
//
//   - []entities.BillingAddress: A slice of billing addresses associated with the member.
//   - error: An error, if any, during the retrieval process.
func (member *MemberRepo) GetAllBillingAddresses(ctx context.Context, memberID uuid.UUID) ([]entities.BillingAddress, error) {
	// Check if given memberID exists
	memberExists, err := member.IsMemberExists(memberID, ctx)
	if err != nil {
		return nil, err
	}
	if !memberExists {
		return nil, errors.New("Member not found") // Return custom error message
	}

	// Initialize a slice to store billing addresses
	var billingAddresses []entities.BillingAddress

	// SQL query to retrieve billing addresses for a specific member_id
	query := `
       SELECT COALESCE(address, ''), COALESCE(zip, ''), 
       COALESCE(country_code, ''), COALESCE(state_code, ''), is_primary_billing
       FROM member_billing_address
       WHERE member_id = $1
    `

	rows, err := member.db.QueryContext(ctx, query, memberID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var billingAddress entities.BillingAddress
		err := rows.Scan(
			&billingAddress.Address,
			&billingAddress.Zipcode,
			&billingAddress.Country,
			&billingAddress.State,
			&billingAddress.Primary,
		)
		if err != nil {
			return nil, err
		}

		// Check if all fields (except is_primary_billing) are NULL
		if billingAddress.Address == "" && billingAddress.Zipcode == "" && billingAddress.Country == "" && billingAddress.State == "" {
			return nil, errors.New("No record found")
		}

		// Append the retrieved billing address to the slice
		billingAddresses = append(billingAddresses, billingAddress)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return billingAddresses, nil
}

// CheckEmailExists checks if an email address already exists in the database.

// Parameters:

//		@ ctx:   The context for the database operation.
//		@ email: The email string to be checked.

// Returns:

// @ emailVal: The email value found in the database (if it exists).
// @ err:      An error, if any, during the database operation.
func (member *MemberRepo) CheckEmailExists(ctx context.Context, email string) (bool, error) {
	var exists bool
	// SQL query for checks if an email address already exists.
	checkEmailExistsQ := `SELECT EXISTS (SELECT 1 FROM member WHERE email = $1)`

	row := member.db.QueryRowContext(ctx, checkEmailExistsQ, email)

	err := row.Scan(&exists)

	if err != nil {
		return false, err
	}

	return exists, nil
}

// RegisterMember registers a new member in the repository.

// Parameters:
//
//	@ ctx: The context for the database operation.
//	@ args: An instance of the Member struct containing the member's information
//	  including first name, last name, email, password, terms condition status,
//	  and tax payment status.
//
// Returns:
//
//	@ err: An error, if any, during the database operation.
func (member *MemberRepo) RegisterMember(ctx context.Context, args entities.Member) (err error) {
	// Hash the password before storing it in the database.

	var hashedPassword string
	if args.Provider == consts.ProviderInternal {
		hashedPassword, err = crypto.Hash(args.Password)

		if err != nil {
			return
		}
	}

	/*----------------------Remove this code in future---------------------*/
	//currently using system partner_id.In future fetch partner_id from jwt token after authentication.
	partnerID, err := uuid.Parse("96b05633-69d7-4144-b0f9-73765b9ea7ca")
	if err != nil {
		return
	}

	/*----------------------Remove this code in future---------------------*/

	getOauthProviderID := `SELECT id FROM oauth_provider WHERE name = $1`
	row := member.db.QueryRowContext(ctx, getOauthProviderID, args.Provider)

	var providerId uuid.UUID
	err = row.Scan(&providerId)

	if err != nil {
		return
	}

	// SQL query for inserting a new member record.
	insertQry := fmt.Sprintf(`INSERT INTO member 
				(firstname,lastname,email,password,is_terms_condition_checked,is_paying_tax,partner_id,oauth_provider_id)
				values(%s)`, utils.PreparePlaceholders(8))

	_, err = member.db.Exec(insertQry, args.FirstName,
		args.LastName, args.Email, hashedPassword,
		args.TermsConditionChecked, args.PayingTax,
		partnerID, providerId,
	)

	// Return any error encountered during the database operation.
	if err != nil {
		return
	}

	return
}

// ViewMemberProfile retrieves a member's profile information, including billing addresses,
// from the database.
//
// Parameters:
//
//	@memberId: The unique identifier of the member whose profile is being retrieved.
//	@ctx: The context for the database operation.
//
// Returns:
//
//	@memberProfile: An instance of entities.MemberProfile containing the member's profile details.
//	@err: An error, if any, encountered during the database operation.
func (member *MemberRepo) ViewMemberProfile(memberId uuid.UUID, ctx context.Context) (entities.MemberProfile, error) {

	getMemberProfileQ := `
		SELECT
			COALESCE(m.title, ''),
			COALESCE(m.firstname, ''),
			COALESCE(m.lastname, ''),
			COALESCE(m.gender, ''),
			m.email,
			COALESCE(m.mobile, ''),
			COALESCE(m.address1, ''),
			COALESCE(m.address2, ''),
			COALESCE(m.country_code, ''),
			COALESCE(m.state_code, ''),
			COALESCE(m.city, ''),
			COALESCE(m.zip, ''),
			COALESCE(m.language_code, ''),
			m.is_mail_subscribed,
			COALESCE(mba.address, ''),
			COALESCE(mba.zip, '') as ba_zip,
			COALESCE(mba.country_code, '') as ba_country,
			COALESCE(mba.state_code, '') as ba_state,
			mba.is_primary_billing
		FROM
			member m
		LEFT JOIN
			member_billing_address mba
		ON
			mba.member_id = m.id
		WHERE
			m.id = $1
	`

	var (
		memberProfile    entities.MemberProfile
		billingAddresses []entities.BillingAddress
	)

	rows, err := member.db.QueryContext(ctx, getMemberProfileQ, memberId)

	if err != nil {
		return memberProfile, err
	}

	defer rows.Close()

	for rows.Next() {
		var billingAddress entities.BillingAddress
		err := rows.Scan(
			&memberProfile.MemberDetails.Title,
			&memberProfile.MemberDetails.FirstName,
			&memberProfile.MemberDetails.LastName,
			&memberProfile.MemberDetails.Gender,
			&memberProfile.MemberDetails.Email,
			&memberProfile.MemberDetails.Phone,
			&memberProfile.MemberDetails.Address1,
			&memberProfile.MemberDetails.Address2,
			&memberProfile.MemberDetails.Country,
			&memberProfile.MemberDetails.State,
			&memberProfile.MemberDetails.City,
			&memberProfile.MemberDetails.Zipcode,
			&memberProfile.MemberDetails.Language,
			&memberProfile.EmailSubscribed,
			&billingAddress.Address,
			&billingAddress.Zipcode,
			&billingAddress.Country,
			&billingAddress.State,
			&billingAddress.Primary,
		)

		if err != nil {
			return memberProfile, err
		}

		billingAddresses = append(billingAddresses, billingAddress)
		memberProfile.MemberBillingAddress = billingAddresses

	}

	if err := rows.Err(); err != nil {
		return memberProfile, err
	}

	return memberProfile, nil
}

// ViewMembers retrieves a list of member profiles with additional information,
// such as their roles, partner names, album, track, and artist counts, from the database.
//
// Parameters:
//
//	@ctx: The context for the database operation.
//	@params: An instance of entities.Params containing filtering and pagination parameters.
//
// Returns:
//
//	@members: A slice of entities.ViewMembers, each representing a member's profile.
//	@err: An error, if any, encountered during the database operation.
func (member *MemberRepo) ViewMembers(ctx context.Context, params entities.Params) ([]entities.ViewMembers, error) {
	members := make([]entities.ViewMembers, 0)
	viewMembersQ := `
		SELECT
			m.id,
			CONCAT(m.firstname, ' ', m.lastname) AS memberName,
			m.member_role_id,
			l.name AS roleName,
			p.name AS partnerName,
			m.email,
			m.country_code,
			c.name AS countryName,
			m.is_active,
			(
				SELECT COUNT(id)
				FROM product
				WHERE member_id = m.id
				AND is_active = $1
				AND is_deleted = $2
			) AS albumCount,
			(
				SELECT COUNT(id)
				FROM track
				WHERE member_id = m.id
				AND is_deleted = $3
			) AS trackCount,
			(
				SELECT COUNT(id)
				FROM artist
				WHERE member_id = m.id
				AND is_active = $4
				AND is_deleted = $5
			) AS artistCount
		FROM
			member m
		INNER JOIN
			lookup l
		ON
			l.id = m.member_role_id
		INNER JOIN
			partner p
		ON
			p.id = m.partner_id
		LEFT JOIN
			country c
		ON
			c.iso = m.country_code
		WHERE
			m.is_deleted = $6
	`

	if params.Status != "" {
		if params.Status == consts.Active {
			viewMembersQ = fmt.Sprintf("%s AND m.is_active = %s", viewMembersQ, "true")
		} else if params.Status == consts.Inactive {
			viewMembersQ = fmt.Sprintf("%s AND m.is_active = %s", viewMembersQ, "false")
		}
	}

	if params.Partner != "" && params.Partner != "-1" {
		partner, err := uuid.Parse(params.Partner)
		if err != nil {
			return members, err
		}
		viewMembersQ = fmt.Sprintf("%s AND m.partner_id = '%s'", viewMembersQ, partner)
	}

	if params.Role != "" && params.Role != "-1" {
		role, err := strconv.Atoi(params.Role)
		if err != nil {
			return members, err
		}
		viewMembersQ = fmt.Sprintf("%s AND m.member_role_id =%d", viewMembersQ, role)
	}

	if params.Search != "" {
		viewMembersQ = fmt.Sprintf("%s AND (p.name LIKE '%s' OR CONCAT(m.firstname, ' ', m.lastname) LIKE '%s' OR m.email like '%s')", viewMembersQ, params.Search, params.Search, params.Search)
	}

	if params.SortBy != "" {
		viewMembersQ = fmt.Sprintf("%s ORDER BY m.%s ASC", viewMembersQ, params.SortBy)
	}

	if params.Page != "" && params.Limit != "" {
		page, err := strconv.Atoi(params.Page)
		if err != nil {
			return members, err
		}

		limit, err := strconv.Atoi(params.Limit)
		if err != nil {
			return members, err
		}

		offset := (page - 1) * limit
		viewMembersQ = fmt.Sprintf("%s OFFSET %d", viewMembersQ, offset)
	}

	if params.Limit != "" {
		viewMembersQ = fmt.Sprintf("%s LIMIT %s", viewMembersQ, params.Limit)
	}

	rows, err := member.db.QueryContext(ctx, viewMembersQ, true, false, false, true, false, false)

	if err != nil {
		return members, err
	}

	defer rows.Close()

	for rows.Next() {
		var member entities.ViewMembers
		err := rows.Scan(
			&member.MemberId,
			&member.Name,
			&member.Role.Id,
			&member.Role.Name,
			&member.PartnerName,
			&member.Email,
			&member.Country.Code,
			&member.Country.Name,
			&member.Active,
			&member.AlbumCount,
			&member.TrackCount,
			&member.ArtistCount,
		)

		if err != nil {
			return members, err
		}

		members = append(members, member)
	}

	if err := rows.Err(); err != nil {
		return members, err
	}

	return members, nil

}

//GetBasicMemberDetailsByEmail retrieves basic member details from the database.
//
// Parameters:
//   - args (entities.MemberPayload): An instance of entities.MemberPayload containing member information.
//   - ctx (context.Context): The context for the database operation.
//
// Returns:
//   - basicMemberData (entities.BasicMemberData): A struct containing the basic member details.
//   - error: An error, if any, encountered during the database operation.

func (member *MemberRepo) GetBasicMemberDetailsByEmail(args entities.MemberPayload, ctx context.Context) (entities.BasicMemberData, error) {

	var basicMemberData entities.BasicMemberData

	getBasicMemberDataQ := `
		SELECT
			m.id,
			CONCAT(m.firstname, ' ', m.lastname) AS memberName,
			p.name,
			p.id,
			m.email,
			l.name AS user_type,
			ar.name AS user_roles
		FROM
			member m
		INNER JOIN
			partner p
			ON p.id = m.partner_id
		INNER JOIN
			lookup l
			ON l.id = m.member_role_id
		INNER JOIN
			member_access_role mar
			ON mar.member_id = m.id
		INNER JOIN
			access_role ar
			ON ar.id = mar.role_id
		WHERE
			m.partner_id = $1
		AND 
			m.email = $2
	`

	hashedPassword, err := crypto.Hash(args.Password)
	if err != nil {
		return basicMemberData, err
	}

	if args.Provider == consts.ProviderInternal {
		getBasicMemberDataQ = fmt.Sprintf("%s AND m.password = '%s'", getBasicMemberDataQ, hashedPassword)
	}

	rows, err := member.db.QueryContext(ctx, getBasicMemberDataQ, args.PartnerID, args.Email)

	if err != nil {
		return basicMemberData, err
	}

	defer rows.Close()

	for rows.Next() {
		var roles sql.NullString // Use sql.NullString to handle potential NULL values

		err := rows.Scan(
			&basicMemberData.MemberID,
			&basicMemberData.Name,
			&basicMemberData.PartnerName,
			&basicMemberData.PartnerID,
			&basicMemberData.Email,
			&basicMemberData.MemberType,
			&roles,
		)
		if err != nil {
			return basicMemberData, err
		}

		// Check for NULL roles and append them if they are not NULL
		if roles.Valid {
			basicMemberData.MemberRoles = append(basicMemberData.MemberRoles, roles.String)
		}
	}

	if err := rows.Err(); err != nil {
		return basicMemberData, err
	}

	return basicMemberData, nil
}
