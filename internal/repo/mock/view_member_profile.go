package mock

import (
	context "context"
	entities "member/internal/entities"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	uuid "github.com/google/uuid"
)

// ViewMemberProfile mocks base method.
func (m *MockMemberRepoImply) ViewMemberProfile(memberId uuid.UUID, ctx context.Context) (entities.MemberProfile, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ViewMemberProfile", memberId, ctx)
	ret0, _ := ret[0].(entities.MemberProfile)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ViewMemberProfile indicates an expected call of ViewMemberProfile.
func (mr *MockMemberRepoImplyMockRecorder) ViewMemberProfile(memberId, ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ViewMemberProfile", reflect.TypeOf((*MockMemberRepoImply)(nil).ViewMemberProfile), memberId, ctx)
}
