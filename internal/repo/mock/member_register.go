package mock

import (
	context "context"
	entities "member/internal/entities"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// CheckEmailExists mocks base method.
func (m *MockMemberRepoImply) CheckEmailExists(ctx context.Context, email string) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CheckEmailExists", ctx, email)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CheckEmailExists indicates an expected call of CheckEmailExists.
func (mr *MockMemberRepoImplyMockRecorder) CheckEmailExists(ctx, email interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CheckEmailExists", reflect.TypeOf((*MockMemberRepoImply)(nil).CheckEmailExists), ctx, email)
}

// RegisterMember mocks base method.
func (m *MockMemberRepoImply) RegisterMember(ctx context.Context, args entities.Member) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RegisterMember", ctx, args)
	ret0, _ := ret[0].(error)
	return ret0
}

// RegisterMember indicates an expected call of RegisterMember.
func (mr *MockMemberRepoImplyMockRecorder) RegisterMember(ctx, args interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RegisterMember", reflect.TypeOf((*MockMemberRepoImply)(nil).RegisterMember), ctx, args)
}
