package mock

import (
	context "context"
	entities "member/internal/entities"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// ViewMembers mocks base method.
func (m *MockMemberRepoImply) ViewMembers(ctx context.Context, params entities.Params) ([]entities.ViewMembers, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ViewMembers", ctx, params)
	ret0, _ := ret[0].([]entities.ViewMembers)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ViewMembers indicates an expected call of ViewMembers.
func (mr *MockMemberRepoImplyMockRecorder) ViewMembers(ctx, params interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ViewMembers", reflect.TypeOf((*MockMemberRepoImply)(nil).ViewMembers), ctx, params)
}
