package mock

import (
	context "context"
	entities "member/internal/entities"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// GetBasicMemberDetailsByEmail mocks base method.
func (m *MockMemberRepoImply) GetBasicMemberDetailsByEmail(args entities.MemberPayload, ctx context.Context) (entities.BasicMemberData, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetBasicMemberDetailsByEmail", args, ctx)
	ret0, _ := ret[0].(entities.BasicMemberData)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetBasicMemberDetailsByEmail indicates an expected call of GetBasicMemberDetailsByEmail.
func (mr *MockMemberRepoImplyMockRecorder) GetBasicMemberDetailsByEmail(args, ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetBasicMemberDetailsByEmail", reflect.TypeOf((*MockMemberRepoImply)(nil).GetBasicMemberDetailsByEmail), args, ctx)
}
