package mock

import (
	gomock "github.com/golang/mock/gomock"
)

// MockMemberRepoImply is a mock of MemberRepoImply interface.
type MockMemberRepoImply struct {
	ctrl     *gomock.Controller
	recorder *MockMemberRepoImplyMockRecorder
}

// MockMemberRepoImplyMockRecorder is the mock recorder for MockMemberRepoImply.
type MockMemberRepoImplyMockRecorder struct {
	mock *MockMemberRepoImply
}

// NewMockMemberRepoImply creates a new mock instance.
func NewMockMemberRepoImply(ctrl *gomock.Controller) *MockMemberRepoImply {
	mock := &MockMemberRepoImply{ctrl: ctrl}
	mock.recorder = &MockMemberRepoImplyMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockMemberRepoImply) EXPECT() *MockMemberRepoImplyMockRecorder {
	return m.recorder
}
