package middlewares

import (
	"encoding/json"
	"fmt"
	"io"
	"member/internal/consts"
	"member/internal/entities"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/utils"
)

// Middlewares structure for storing middleware values
type Middlewares struct {
	Cfg *entities.EnvConfig
}

// NewMiddlewares creates a new middleware object
func NewMiddlewares(cfg *entities.EnvConfig) *Middlewares {
	return &Middlewares{
		Cfg: cfg,
	}
}

// LocalizationLanguage
func (m Middlewares) LocalizationLanguage() gin.HandlerFunc {
	return func(c *gin.Context) {
		// check it is exists in the header
		lan := c.Request.Header.Get(consts.HeaderLocallizationLanguage)
		if lan == "" {
			lan = ""
		}

		// setting the language
		c.Set(consts.ContextLocallizationLanguage, lan)

		c.Next()
	}
}

// Middleware funcion for error localization
func (m Middlewares) ErrorLocalization() gin.HandlerFunc {
	return func(c *gin.Context) {
		//for storing error response data from context
		var errorData = make(map[string]any)

		localisationUrl := fmt.Sprintf("%s/localization/error",
			m.Cfg.LocalisationServiceURL)
		language, ok := utils.GetContext[string](c, consts.ContextLocallizationLanguage)
		if !ok {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "Unable to find the localization language"})
			return
		}
		if language == "" {
			language = "en"
		}

		headers := map[string]any{
			consts.HeaderLocallizationLanguage: language,
		}

		resp, err := utils.APIRequest(http.MethodGet, localisationUrl, headers, nil)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "An unexpected error occured"})
			return
		}

		//checking the status code
		if resp.StatusCode != http.StatusOK {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "unable to read data from localisation response",
			})
			return
		}
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": fmt.Errorf("unable to read data from localisation response %v",
					err).Error(),
			})
			return
		}
		defer resp.Body.Close()

		err = json.Unmarshal(body, &errorData)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "unable to read data from localisation response",
			})
			return
		}

		// set error data in context
		c.Set(consts.ContextErrorResponses, errorData)

		c.Next()
	}
}

// Middleware function to get endpointnames
func (m Middlewares) EndPointNames() gin.HandlerFunc {

	return func(c *gin.Context) {
		var endpoints models.ResponseData

		getEndpointUrl := fmt.Sprintf("%s/localization/endpointname",
			m.Cfg.EndpointURL)
		language, ok := utils.GetContext[string](c, consts.ContextLocallizationLanguage)
		if !ok {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "Unable to find the localization language"})
			return
		}

		headers := map[string]interface{}{
			consts.HeaderLocallizationLanguage: language,
		}

		resp, err := utils.APIRequest(http.MethodGet, getEndpointUrl, headers, nil)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "An unexpected error occured"})
			return
		}

		//checking the status code
		if resp.StatusCode != http.StatusOK {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "unable to read data from localisation response",
			})
			return
		}
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": fmt.Errorf("unable to read data from localisation response %v",
					err).Error(),
			})
			return
		}
		defer resp.Body.Close()

		err = json.Unmarshal(body, &endpoints)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "unable to read data from localisation response",
			})
			return
		}

		// set error data in context
		c.Set(consts.ContextEndPoints, endpoints)

		c.Next()
	}
}
